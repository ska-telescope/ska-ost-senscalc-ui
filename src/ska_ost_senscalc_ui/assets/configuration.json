{
    "appTitle": [{
        "name": "Sensitivity Calculator"
    }],
    "currentVersion": "1.5.0",
    "documentationMidURL": "https://docs.google.com/document/d/1ppt2T2d67HB3iZLb6pXAjAG3oiw5nZ3NmtTmJROqJsg/edit#heading=h.gjdgxs",
    "documentationLowURL": "https://docs.google.com/document/d/1tB-5ZwpYdlavyTyELrZWpFnEJgPv2Qc4azONxlU5kXM/edit#heading=h.fn3j4uie4zpn",
    "skaoHomepage": "https://www.skatelescope.org/",

    "midCalcFormFields": [
        {
            "field": "fieldObservingMode",
            "title": "Observing Band",
            "description": "The frequency band that will be used",
            "defaultValue" : [
                {
                    "mode": "Band 1",
                    "description": "(0.35 - 1.05 GHz)",
                    "contFreqInput": [
                      {"type": "ska", "value": 0.7},
                      {"type": "meerkat", "value": 0.7975},
                      {"type": "mixed", "value": 0.7975}
                    ],
                    "contBandInput": [
                      {"type": "ska", "value": 0.7},
                      {"type": "meerkat", "value": 0.435},
                      {"type": "mixed", "value": 0.435}
                    ],
                    "selected": true,
                    "bandLimits": [
                      {"type": "ska", "limits": [0.35e9, 1.05e9] },
                      {"type": "meerkat", "limits": [0.58e9, 1.015e9]},
                      {"type": "mixed", "limits": [0.58e9, 1.015e9]}
                    ],
                    "zoomFreqInput": [
                      {"type": "ska", "value": 0.7},
                      {"type": "meerkat", "value": 0.7975},
                      {"type": "mixed", "value": 0.7975}
                    ]
                },
                {
                    "mode": "Band 2",
                    "description": "(0.95 - 1.76 GHz)",
                    "contFreqInput": [
                      { "type": "ska", "value": 1.355},
                      { "type": "meerkat", "value": 1.31},
                      { "type": "mixed", "value": 1.31}
                    ],
                    "contBandInput": [
                      { "type": "ska", "value": 0.81},
                      { "type": "meerkat", "value": 0.72},
                      { "type": "mixed", "value": 0.72}
                    ],
                    "bandLimits": [
                      { "type": "ska", "limits": [0.95e9, 1.76e9]},
                      {"type": "meerkat", "limits": [0.95e9, 1.67e9]},
                      {"type": "mixed", "limits": [0.95e9, 1.67e9]}
                    ],
                    "zoomFreqInput": [
                      { "type": "ska", "value": 1.355},
                      { "type": "meerkat", "value": 1.31},
                      { "type": "mixed", "value": 1.31}
                    ]
                },
                {
                    "mode": "Band 5a",
                    "description": "(4.6 - 8.5 GHz)",
                    "contFreqInput": [{ "type": "ska", "value": 6.55}],
                    "contBandInput": [{ "type": "ska", "value": 3.9}],
                    "bandLimits": [{"type": "ska", "limits": [4.6e9, 8.5e9]}],
                    "zoomFreqInput": [{ "type": "ska", "value": 6.55}]
                },
                {
                    "mode": "Band 5b",
                    "description": "(8.3 - 15.4 GHz)",
                    "contFreqInput": [{ "type": "ska", "value": 11.85}],
                    "contBandInput": [{ "type": "ska", "value": 5}],
                    "bandLimits": [{ "type": "ska", "limits": [8.3e9, 15.4e9]}],
                    "zoomFreqInput": [{ "type": "ska", "value": 11.85}]
                }
            ]
        },
        {
            "field": "fieldRightAscension",
            "title": "Right Ascension",
            "description": "Right Ascension of the source in sexagesimal format (hr:min:sec)",
            "placeholder": "hh:mm:ss.s",
            "defaultValue" : "00:00:00.0"
        },
        {
            "field": "fieldDeclination",
            "title": "Declination",
            "description": "Declination of the source in sexagesimal format (deg:arcmin:arcsec)",
            "placeholder": "[-]dd:mm:ss.s",
            "defaultValue" : "00:00:00.0"
        },
        {
            "field": "fieldRightAscensionDec",
            "title": "Decimal Right Ascension",
            "description": "Right Ascension in decimal degrees",
            "placeholder": "n.n",
            "defaultValue" : "0.0"
        },
        {
            "field": "fieldDeclinationDec",
            "title": "Decimal Declination",
            "description": "Declination in decimal degrees",
            "placeholder": "n.n",
            "defaultValue" : "0.0"
        },
        {
            "field": "fieldWeatherPWV",
            "title": "Weather (Precipitable Water Vapour)",
            "description": "The weather condition for observing, PWV (mm) between 3 and 25",
            "placeholder": "Enter value...",
            "defaultValue" : {
                "value": 10,
                "min": 3,
                "max": 25
            }
        },
        {
            "field": "fieldElevation",
            "title": "Elevation",
            "description": "Antenna elevation in degrees",
            "placeholder": "Enter value...",
            "defaultValue" : {
                "value": 45,
                "min": 15,
                "max": 90
            }
        },
        {
            "field": "fieldArrayConfig",
            "title": "Subarray Configuration",
            "description": "Antenna/subarray selection",
            "defaultValue" : ["MID_AA4_all", "MID_AA4_SKA_only"]
        },
        {
            "field": "fieldnSKA",
            "title": "Number of 15-m antennas",
            "description": "Number of 15-m antennas",
            "placeholder": "Enter value...",
            "defaultValue" : null
        },
        {
            "field": "fieldnMeer",
            "title": "Number of 13.5-m antennas",
            "description": "Number of 13.5-m antennas",
            "placeholder": "Enter value...",
            "defaultValue" : null
        },
        {
            "field": "fieldCentralFrequency",
            "title": "Central Frequency",
            "description": "The central frequency of the observation",
            "placeholder": "Enter value...",
            "defaultValue" : 6.55
        },
        {
            "field": "fieldBandwidth",
            "title": "Continuum Bandwidth",
            "description": "The total bandwidth of the continuum observation",
            "placeholder": "Enter value...",
            "defaultValue" : 0.435
        },
        {
            "field": "fieldResolution",
            "title": "Spectral Resolution",
            "description": "Channel spacing of continuum data",
            "placeholder": "",
            "defaultValue" : "13.44 kHz"
        },
        {
            "field": "fieldEffectiveResolution",
            "title": "Effective resolution",
            "description": "Spectral resolution after averaging",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldSpectralAveraging",
            "title": "Spectral Averaging",
            "description": "Factor by which the intrinsic resolution should be averaged",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldNoOfSubBands",
            "title": "Number of sub-bands",
            "description": "Divide the bandwidth into a number of sub-bands to have a sensitivity reported for each sub-band.",
            "placeholder": "Enter value...",
            "defaultValue" : {
                "value": 1,
                "min": 1,
                "max": 32
            }
        },
        {
            "field": "fieldIntegrationTime",
            "title": "Integration Time",
            "description": "On-source integration time",
            "placeholder": "Enter value...",
            "defaultValue" : 600
        },
        {
            "field": "fieldSensitivity",
            "title": "Sensitivity",
            "description": "Sensitivity",
            "placeholder": "Enter value...",
            "defaultValue" : 1
        },
        {
            "field": "fieldSupplied",
            "title": "Supplied",
            "description": "",
            "placeholder": "",
            "defaultValue" : [
                {
                    "value": "IntegrationTime",
                    "displayValue": "Integration Time",
                    "selected": true
                },
                {
                    "value": "Sensitivity",
                    "displayValue": "Sensitivity"
                }
            ]
        },
        {
            "field": "fieldZoomFrequency",
            "title": "Zoom Frequency",
            "description": "Central frequency of the zoom window",
            "placeholder": "Enter value...",
            "defaultValue" : 6.55
        },
        {
            "field": "fieldZoomResolution",
            "title": "Zoom Bandwidth and Spectral Resolution",
            "description": "Total bandwidth and channel spacing of the zoom window",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldEffectiveResolution",
            "title": "Zoom Effective Resolution",
            "description": "Spectral resolution after averaging",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldZoomSpectralAveraging",
            "title": "Zoom Spectral Averaging",
            "description": "Factor by which the intrinsic resolution should be averaged",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldImageWeighting",
            "title": "Image Weighting",
            "description": "Select an image weighting option",
            "placeholder": "",
            "defaultValue" : [
                {
                    "option": "Natural",
                    "value": "natural",
                    "selected": false,
                    "robust": false
                },
                {
                    "option": "Uniform",
                    "value": "uniform",
                    "selected": true,
                    "robust": false
                },
                {
                    "option": "Briggs",
                    "value": "robust",
                    "selected": false,
                    "robust": true
                }]
        },
        {
            "field": "fieldTapering",
            "title": "Tapering",
            "description": "Select a u,v taper (defined as arcsec in the image plane)",
            "placeholder": "",
            "defaultValue" : [
              [0, 0.25, 1, 4, 16, 64, 256, 1024],
              {
                "AA0.5": [0, 0.25, 1, 4, 16, 64],
                "AA1": [0, 0.25, 1, 4, 16, 64]
              }
            ]
        },
        {
            "field": "fieldRobust",
            "title": "Robust",
            "description": "Select a robustness",
            "placeholder": "",
            "defaultValue" : [-2,-1,0,1,2]
        }

    ],
    "advancedModeFields": [
        {
            "field": "fieldToggleMode",
            "title": "Advanced Mode",
            "description": "",
            "defaultValue" : null
        },
        {
            "field": "fieldEtaSystem",
            "title": "system",
            "description": "System Efficiency",
            "placeholder": "",
            "defaultValue" : {
                "min": 0,
                "max": 1
            }
        },
        {
            "field": "fieldEtaPointing",
            "title": "pointing",
            "description": "Pointing Efficiency",
            "placeholder": "",
            "defaultValue" : {
                "min": 0,
                "max": 1
            }
        },
        {
            "field": "fieldEtaCoherence",
            "title": "coherence",
            "description": "Coherence Efficiency",
            "placeholder": "",
            "defaultValue" : {
                "min": 0,
                "max": 1
            }
        },
        {
            "field": "fieldEtaDigitisation",
            "title": "digitisation",
            "description": "Digitisation Efficiency",
            "placeholder": "",
            "defaultValue" : {
                "min": 0,
                "max": 1
            }
        },
        {
            "field": "fieldEtaCorrelation",
            "title": "correlation",
            "description": "Correlation Efficiency",
            "placeholder": "",
            "defaultValue" : {
                "min": 0,
                "max": 1
            }
        },
        {
            "field": "fieldEtaBandpass",
            "title": "bandpass",
            "description": "Bandpass Efficiency",
            "placeholder": "",
            "defaultValue" : {
                "min": 0,
                "max": 1
            }
        },
        {
            "field": "fieldTsysSKA",
            "title": "sys 15-m",
            "description": "15-m antennas System temperature (K)",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldTrcvSKA",
            "title": "rcv 15-m",
            "description": "15-m antennas Receiver temperature (K)",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldTsplSKA",
            "title": "spl 15-m",
            "description": "15-m antennas Spillover temperature (K)",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldTsysMeerKAT",
            "title": "sys 13.5-m",
            "description": "13.5-m antennas System temperature (K)",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldTrcvMeerKAT",
            "title": "rcv 13.5-m",
            "description": "13.5-m antennas Receiver temperature (K)",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldTsplMeerKAT",
            "title": "spl 13.5-m",
            "description": "13.5-m antennas Spillover temperature (K)",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldTsky",
            "title": "sky",
            "description": "Sky brightness temperature (K)",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldTgal",
            "title": "gal",
            "description": "Galactic contribution to sky brightness temperature(K)",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldTalpha",
            "title": "alpha",
            "description": "Spectral Index of Galactic Emission",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldEtaSKA",
            "title": "dish 15-m",
            "description": "Efficiency of 15-m dishes",
            "placeholder": "",
            "defaultValue" : null
        },
        {
            "field": "fieldEtaMeer",
            "title": "dish 13.5-m",
            "description": "Efficiency of 13.5-m dishes",
            "placeholder": "",
            "defaultValue" : null
        }
    ],
    "dropDownValues": [
        {
            "type": "time",
            "value": "ms",
            "operator": "/",
            "multiplier": 1000
        },
        {
            "type": "time",
            "value": "us",
            "operator": "/",
            "multiplier": 1000000
        },
        {
            "type": "time",
            "value": "ns",
            "operator": "/",
            "multiplier": 1000000000
        },
        {
            "type": "time",
            "value": "min",
            "operator": "*",
            "multiplier": 60
        },
        {
            "type": "time",
            "value": "h",
            "operator": "*",
            "multiplier": 3600
        },
        {
            "type": "time",
            "value": "d",
            "operator": "*",
            "multiplier": 86400
        },
        {
            "type": "time",
            "value": "s",
            "operator": "*",
            "multiplier": 1,
            "selected": true
        },
        {
            "type": "frequency",
            "value": "Hz",
            "operator": "*",
            "multiplier": 1
        },
        {
            "type": "frequency",
            "value": "GHz",
            "operator": "*",
            "multiplier": 1000000000,
            "selected": true
        },
        {
            "type": "frequency",
            "value": "MHz",
            "operator": "*",
            "multiplier": 1000000
        },
        {
            "type": "frequency",
            "value": "kHz",
            "operator": "*",
            "multiplier": 1000
        },
        {
            "type": "sensitivity",
            "value": "Jy/beam",
            "operator": "*",
            "multiplier": 1,
            "selected": true
        },
        {
            "type": "sensitivity",
            "value": "mJy/beam",
            "operator": "/",
            "multiplier": 1000
        },
        {
            "type": "sensitivity",
            "value": "uJy/beam",
            "operator": "/",
            "multiplier": 1000000
        },
        {
            "type": "sensitivity",
            "value": "nJy/beam",
            "operator": "/",
            "multiplier": 1000000000
        },
        {
            "type": "sensitivity",
            "value": "K",
            "operator": "*",
            "multiplier": 1
        },
        {
            "type": "sensitivity",
            "value": "mK",
            "operator": "/",
            "multiplier": 1000
        },
        {
            "type": "sensitivity",
            "value": "uK",
            "operator": "/",
            "multiplier": 1000000
        }
    ],
    "validationErrors": [
        {
            "errorName" : "raError",
            "errorMessage": "Input formatted incorrectly"
        },
        {
            "errorName" : "decError",
            "errorMessage": "Input formatted incorrectly"
        },
      {
            "errorName" : "isVisibleError",
            "errorMessage": "Target does not reach minimum elevation of 15 degrees"
        },
        {
            "errorName" : "numericError",
            "errorMessage": "Please enter a number"
        },
        {
            "errorName" : "negativeError",
            "errorMessage": "Value cannot be negative"
        },
        {
            "errorName" : "integerError",
            "errorMessage": "Must be an integer value"
        },
        {
            "errorName" : "minmaxError",
            "errorMessage": "Please enter a number between %s and %s"
        },
        {
            "errorName" : "efficiencyError",
            "errorMessage": "Please enter a number greater than 0 and less than or equal to 1"
        },
        {
            "errorName" : "greaterThanZeroError",
            "errorMessage": "Please enter a value greater than 0"
        },        {
            "errorName" : "minReceiverError",
            "errorMessage": "Minimum of 2 antennas needed"
        },
        {
            "errorName" : "rangeError",
            "errorMessage": "Observing frequency out of range"
        },
        {
            "errorName": "bandwidthRangeError",
            "errorMessage": "Bandwidth must be fully contained within the band"
        },
        {
            "errorName": "pulseWidthError",
            "errorMessage": "Intrinsic pulse width cannot be greater than pulse period"
        },
        {
            "errorName": "contBandwidthMaximumExceeded",
            "errorMessage": "Maximum bandwidth for this array assembly (%s MHz) exceeded"
        },
        {
            "errorName": "bandwidthSmallerThanChannel",
            "errorMessage": "Bandwidth must be larger than the channel width (13.44 kHz)"
        },
        {
            "errorName": "minimumValueError",
            "errorMessage": "Please enter a value greater than %s"
        }
    ],
    "resolutionDropdownValues":[
        {
            "value": 0.21,
            "displayValue": "3.125 MHz %s, 0.21 kHz %s",
            "bandwidth": 3125000,
            "selected": true
        },
        {
            "value": 0.42,
            "displayValue": "6.25 MHz %s, 0.42 kHz %s",
            "bandwidth": 6250000

        },
        {
            "value": 0.84,
            "displayValue": "12.5 MHz %s, 0.84 kHz %s",
            "bandwidth": 12500000
        },
        {
            "value": 1.68,
            "displayValue": "25 MHz %s, 1.68 kHz %s",
            "bandwidth": 25000000
        },
        {
            "value": 3.36,
            "displayValue": "50 MHz %s, 3.36 kHz %s",
            "bandwidth": 50000000
        },
        {
            "value": 6.72,
            "displayValue": "100 MHz %s, 6.72 kHz %s",
            "bandwidth": 100000000
        },
        {
            "value": 13.44,
            "displayValue": "200 MHz %s, 13.44 kHz %s",
            "bandwidth": 200000000
        }
    ],
    "spectralAveragingValues": [
        {
            "value": 1,
            "selected": true
        },
        {
            "value": 2
        },
        {
            "value": 3
        },
        {
            "value": 4
        },
        {
            "value": 6
        },
        {
            "value": 8
        },
        {
            "value": 12
        },
        {
            "value": 24
        }
    ],
    "pulsarSearchArrays": [
        {
            "label": "AA2 (core only)"
        },
        {
            "label": "AA* (core only)"
        },
        {
            "label": "AA4 (core only)"
        }
    ],
    "advancedModeActive": true
}
