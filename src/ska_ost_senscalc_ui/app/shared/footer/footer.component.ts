import { Component, OnInit } from '@angular/core';
import { AppConfiguration, Configuration } from '../../app-configuration';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  year: string;
  config!: Configuration;

  constructor(private configService: AppConfiguration) {
    this.year = "";
    const serviceConfig = this.configService.getConfig();
    if (serviceConfig) {
      this.config = serviceConfig;
    }
  }

  ngOnInit(): void {
    this.year = new Date().getFullYear().toString();
  }

  getYear(): string {
    return this.year;
  }

  getVersion(): string {
    return this.config.currentVersion;
  }

}
