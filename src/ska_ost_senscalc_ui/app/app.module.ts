import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';

import { AppConfiguration } from './app-configuration';
import { AlwaysReuseStrategy, AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { BASE_PATH as LOW_BASE_PATH, LowApiModule } from './generated/api/low';
import { BASE_PATH as MID_BASE_PATH, MidApiModule } from './generated/api/mid';
import { SharedModule } from './shared/shared.module';

export function initConfig(appConfigService: AppConfiguration) {
  return () =>
    appConfigService
      .loadConfig()
      .then(() => appConfigService.loadRuntimeConfig());
}

export function apiBasePath(config: AppConfiguration) {
  return `${config.getRuntimeConfig().backendURL}/api/v10`;
}

export function openLowApiBasePath(config: AppConfiguration) {
  return `${apiBasePath(config)}/low`;
}

export function openMidApiBasePath(config: AppConfiguration) {
  return `${apiBasePath(config)}/mid`;
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    LowApiModule,
    MidApiModule,
    HttpClientModule
  ],
  providers: [
    AppConfiguration,
    {
      provide: APP_INITIALIZER,
      useFactory: initConfig,
      deps: [AppConfiguration],
      multi: true
    },
    {
      provide: LOW_BASE_PATH,
      useFactory: openLowApiBasePath,
      deps: [AppConfiguration]
    },
    {
      provide: MID_BASE_PATH,
      useFactory: openMidApiBasePath,
      deps: [AppConfiguration]
    },
    {
      provide: RouteReuseStrategy,
      useClass: AlwaysReuseStrategy
    }
],
  bootstrap: [AppComponent]
})
export class AppModule { }
