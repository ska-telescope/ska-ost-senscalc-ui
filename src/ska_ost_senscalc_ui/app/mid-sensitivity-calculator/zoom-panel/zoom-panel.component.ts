import {
  AfterViewInit,
  Component,
  Input,
  OnChanges,
  OnInit, SimpleChanges,
  ViewChild
} from '@angular/core';
import {
  AbstractControl,
  UntypedFormBuilder,
  UntypedFormControl,
  UntypedFormGroup,
  Validators
} from '@angular/forms';
import {
  AppConfiguration, ArrayDropDownValue,
  Configuration,
  ConfigurationDropDownValue,
  ConfigurationMidCalculatorFormField,
  ConfigurationResolutionDropDownValue,
  ConfigurationSpectralAvgDropDownValue
} from '../../app-configuration';
import { faCaretDown, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { ValidatorsService } from '../../core/validators.service';
import { ZoomResultsComponent } from './zoom-results/zoom-results.component';
import { LineFormData } from '../types';
import { MidCalculatorService } from '../../core/mid-calculator.service';
import constants from '../../shared/utils/constants';
import helpers from '../../shared/utils/helpers';
import {ZoomResults} from "../../core/types";

@Component({
  selector: 'app-zoom-panel',
  templateUrl: './zoom-panel.component.html',
  styleUrls: ['./zoom-panel.component.scss']
})
export class ZoomPanelComponent implements OnInit, OnChanges, AfterViewInit {

  _observingMode!: string;
  _subarrayType!: ArrayDropDownValue;

  @Input() zoomResults?: ZoomResults | null;

  @Input() set observingMode(value: string) {
    this._observingMode = value;
  }

  @Input() set subarrayType(value: ArrayDropDownValue) {
    this._subarrayType = value;
  }

  lineForm!: UntypedFormGroup;
  configuration?: Configuration;

  //form field configuration
  fieldZoomFrequency!: ConfigurationMidCalculatorFormField;
  fieldZoomResolution!: ConfigurationMidCalculatorFormField;
  fieldSupplied!: ConfigurationMidCalculatorFormField;
  fieldIntegrationTime!: ConfigurationMidCalculatorFormField;
  fieldSensitivity!: ConfigurationMidCalculatorFormField;
  fieldZoomSpectralAveraging!: ConfigurationMidCalculatorFormField;
  fieldEffectiveResolution!: ConfigurationMidCalculatorFormField;
  fieldImageWeighting!: ConfigurationMidCalculatorFormField;
  fieldRobust!: ConfigurationMidCalculatorFormField;
  fieldTapering!: ConfigurationMidCalculatorFormField;

  //defaults
  defaultSupplied!: any;
  selectedZoomFrequency!: ConfigurationDropDownValue;
  defaultFrequencyDropdownOptions!: ConfigurationDropDownValue[];
  defaultResolutionDropdownOptions!: ConfigurationResolutionDropDownValue[];
  defaultTimeDropdownOptions!: ConfigurationDropDownValue[];
  defaultSensitivityDropdownOptions!: ConfigurationDropDownValue[];
  selectedZoomResolution!: ConfigurationResolutionDropDownValue;
  selectedIntegrationTime!: ConfigurationDropDownValue;
  selectedSensitivity!: ConfigurationDropDownValue;
  zoomFreqInput!: number;
  lineFormInitialValues: any;
  defaultZoomSpectralAveraging!: ConfigurationSpectralAvgDropDownValue;
  spectralAveragingOptions!: ConfigurationSpectralAvgDropDownValue[];
  defaultSelectedImageWeighting!: any;
  showRobustness!: boolean;
  imageWeightingStyle!: string;

  //icons
  faCaretDown: IconDefinition = faCaretDown;

  //Output fields
  formResult: any;
  @ViewChild(ZoomResultsComponent) output!: ZoomResultsComponent;

  constructor(
    private config: AppConfiguration,
    private fb: UntypedFormBuilder,
    private validatorService: ValidatorsService,
    public midCalcService: MidCalculatorService
  ) {
    if (!this.configuration) {
      this.configuration = this.config.getConfig();
    }
  }

  getImageWeightingStyle(): void {
    this.imageWeightingStyle = this.showRobustness ? 'width: 63%;' : 'width: 97%;';
  }

  ngOnInit(): void {
    this.getformFieldsDataFromConfig();
    this.initializeLineForm();
    this.suppliedChangedZoomPanel(this.defaultSupplied?.value);
    this.setEffectiveResolution();
    this.getImageWeightingStyle();
  }

  ngAfterViewInit(): void {
    // used getRawValue to get value of disabled controls on reset
    this.lineFormInitialValues = this.lineForm.getRawValue();
  }

  ngOnChanges(changes: SimpleChanges): void {
    const fieldObservingMode = this.configuration!.midCalcFormFields!.find(
      (e: ConfigurationMidCalculatorFormField) =>
        e.field == 'fieldObservingMode'
    );

    if (fieldObservingMode) {

      const mode = fieldObservingMode.defaultValue.find((e: any) => e.mode === this._observingMode);
      const contZoomInputValues = mode?.zoomFreqInput;
      this.zoomFreqInput = helpers.configuration.getDefaultFromConfigurationFile(contZoomInputValues, this._subarrayType);

      this.setDefaultFrequencyDropdownOptions();

      if (this.lineForm) {
        // If the band changes or the array configuration changes and the user hasn't updated any fields,
        // we want to set the continuum frequency and bandwidth to the default values for that band.
        // However, if the user has edited the fields then changes the subarray, we do not want to overwrite their changes
        const subarrayChangedButFrequencyUntouched =   changes['subarrayType'] && !this.lineForm.get('fieldZoomFrequency')?.touched;
        this.updateFrequencyAndBandwidthValidity();
        if (changes['observingMode'] || subarrayChangedButFrequencyUntouched) {
          this.lineForm.get('fieldZoomFrequency')?.setValue(this.zoomFreqInput);
          this.selectedZoomFrequency = this.getSelectedDropdownValue();
          this.updateFrequencyAndBandwidthValidity();
          this.lineForm.get('fieldZoomFrequency')?.markAsUntouched();
        }
        // check if the chosen taper value is allowed for the current subarray
        // If taper value is not allowed (large taper values are not allowed for
        // some subarrays), set to the largest allowed value
        const tapers = this.getTaperValues();
        if (!tapers.includes(this.lineForm.get('fieldTapering')?.value)) {
          this.lineForm.get('fieldTapering')?.setValue(tapers[tapers.length - 1]);
        }
        this.setEffectiveResolution();
      }
    }
  }

  paramChange() {
    // Add param update message to all relevant result panels.
    // Note: only one panel at this time.
    this.output?.paramChangeZoomResult();
  }

  freqParamChange() {
    this.updateFrequencyAndBandwidthValidity();
    this.setEffectiveResolution();
    this.paramChange();
  }

  updateFrequencyAndBandwidthValidity() {
    this.lineForm.get('fieldZoomFrequency')?.setValidators(
      this.validatorService.centralFrequencyValidator(
        this.selectedZoomFrequency,
        this._observingMode,
        this._subarrayType
      )
    );
    this.lineForm.get('fieldZoomFrequency')?.markAsTouched();
    this.lineForm.get('fieldZoomFrequency')?.updateValueAndValidity();

    // Only validate bandwidth if central frequency is valid
    if (this.lineForm.get('fieldZoomFrequency')?.valid) {
      this.updateBandwidthValidity();
    }
    else {
      this.lineForm.get('fieldZoomResolution')?.setErrors(null);
    }
  }

  updateBandwidthValidity() {
    this.lineForm.setValidators([
      this.validatorService.midZoomBandwidthValidator(
        this.selectedZoomFrequency,
        this._observingMode,
        this._subarrayType
      )
    ]);
    this.lineForm.get('fieldZoomResolution')?.markAsTouched();
    this.lineForm.get('fieldZoomResolution')?.updateValueAndValidity();
    this.lineForm.updateValueAndValidity();
  }

  getMidCalcFormFieldByName(
    name: string
  ): ConfigurationMidCalculatorFormField | undefined {
    return this.configuration!.midCalcFormFields!.find(
      (e: ConfigurationMidCalculatorFormField) => e.field == name
    )!;
  }

  getSelectedDropdownValue(): ConfigurationDropDownValue {
    return this.defaultFrequencyDropdownOptions.find(
      (e: ConfigurationDropDownValue) => e.selected
    )!;
  }

  getFormInputFieldsDataFromConfig(): void {
    this.fieldZoomFrequency = this.getMidCalcFormFieldByName('fieldZoomFrequency')!;
    this.fieldZoomResolution = this.getMidCalcFormFieldByName('fieldZoomResolution')!;
    this.fieldSupplied = this.getMidCalcFormFieldByName('fieldSupplied')!;
    this.fieldZoomSpectralAveraging = this.getMidCalcFormFieldByName('fieldZoomSpectralAveraging')!;
    this.fieldEffectiveResolution = this.getMidCalcFormFieldByName('fieldEffectiveResolution')!;
    this.fieldIntegrationTime = this.getMidCalcFormFieldByName('fieldIntegrationTime')!;
    this.fieldSensitivity =this.getMidCalcFormFieldByName('fieldSensitivity')!;

    this.fieldImageWeighting = this.getMidCalcFormFieldByName('fieldImageWeighting')!;
    this.fieldRobust = this.getMidCalcFormFieldByName('fieldRobust')!;
    this.fieldTapering = this.getMidCalcFormFieldByName('fieldTapering')!;

    if (this.fieldSupplied) {
    this.defaultSupplied = this.fieldSupplied.defaultValue.find((e: any) => e.selected);
    }

    if (this.fieldImageWeighting) {
    this.defaultSelectedImageWeighting = this.fieldImageWeighting.defaultValue.find((e: any) => e.selected);
    }
  }

  getTaperValues(): number[] {
    if (this._subarrayType) {
      return helpers.configuration.getTaperValuesForSubarray(this.fieldTapering, this._subarrayType.label);
    }
    return [];
  }

  getformDropdownFieldsDataFromConfig(): void {
    if (this.configuration?.dropDownValues) {
      this.setDefaultFrequencyDropdownOptions();

      if (this.defaultFrequencyDropdownOptions) {
        this.selectedZoomFrequency = this.getSelectedDropdownValue();
      }

      this.setDefaultTimeDropdownOptions();

      if (this.defaultTimeDropdownOptions) {
        this.selectedIntegrationTime = this.defaultTimeDropdownOptions.find((e: ConfigurationDropDownValue) => e.selected)!;
      }

      this.setDefaultSensitivityDropdownOptions();

      if (this.defaultSensitivityDropdownOptions) {
        this.selectedSensitivity = this.defaultSensitivityDropdownOptions.find((e: ConfigurationDropDownValue) => e.selected)!;
      }
    }
  }

  setDefaultSensitivityDropdownOptions() {
    // We want to list the Janskys in order then the Kelvins
    if (this.configuration?.dropDownValues) {
      const janskyOptions = this.configuration.dropDownValues
        .filter((e: ConfigurationDropDownValue) => e.type === 'sensitivity' && e.value.includes("Jy/beam"))
        .sort(this.unitMagnitudeCompareFn);

      const kelvinOptions = this.configuration.dropDownValues
        .filter((e: ConfigurationDropDownValue) => e.type === 'sensitivity' && e.value.includes("K"))
        .sort(this.unitMagnitudeCompareFn);

      this.defaultSensitivityDropdownOptions = janskyOptions.concat(kelvinOptions);
    }
  }

  setDefaultFrequencyDropdownOptions() {
    if (this.configuration?.dropDownValues) {
      this.defaultFrequencyDropdownOptions = this.configuration.dropDownValues
        .filter((e: ConfigurationDropDownValue) => e.type === 'frequency')
        .sort(this.unitMagnitudeCompareFn);
    }
  }

  setDefaultTimeDropdownOptions() {
    if (this.configuration?.dropDownValues) {
      this.defaultTimeDropdownOptions = this.configuration.dropDownValues
        .filter((e: ConfigurationDropDownValue) => e.type === 'time')
        .sort(this.unitMagnitudeCompareFn);
    }
  }

  unitMagnitudeCompareFn(a: ConfigurationDropDownValue, b: ConfigurationDropDownValue) {
    // This function is used to sort the units in descending order, taking the inverse of the
    // multiplier if the operator is division.
    const first = a.operator == "/" ? (1 / (a.multiplier ?? 1)) : (a.multiplier ?? 1);
    const second = b.operator == "/" ? (1 / (b.multiplier ?? 1)) : (b.multiplier ?? 1);
    return second - first;
  }

  getformFieldsDataFromConfig(): void {
    if (!this.configuration) {
      this.configuration = this.config.getConfig();
    }

    if (this.configuration?.midCalcFormFields) {
      this.getFormInputFieldsDataFromConfig();
    }

    this.getformDropdownFieldsDataFromConfig();
    if (this.configuration?.resolutionDropdownValues) {
      this.defaultResolutionDropdownOptions = this.configuration.resolutionDropdownValues!;
      const resolutionDropdownValues =
        this.defaultResolutionDropdownOptions.find((e: ConfigurationResolutionDropDownValue) => e.selected);
      this.selectedZoomResolution = resolutionDropdownValues!;
    }

    if (this.configuration?.spectralAveragingValues) {
      this.spectralAveragingOptions = this.configuration.spectralAveragingValues;
      this.defaultZoomSpectralAveraging = this.spectralAveragingOptions.find((e: ConfigurationSpectralAvgDropDownValue) => e.selected)!;
    }
  }

  getDefaultValue(field: any) {
    return field ? field.defaultValue : null;
  }

  getValue(field: any) {
    return field ? field.value : null;
  }

  initializeLineForm(): void {
    this.lineForm = this.fb.group(
      {
        fieldZoomFrequency: new UntypedFormControl(
          this.zoomFreqInput ? this.zoomFreqInput : null,
          [Validators.required, this.validatorService.centralFrequencyValidator(
            this.selectedZoomFrequency,
            this._observingMode,
            this._subarrayType
          )]
        ),
        fieldZoomResolution: new UntypedFormControl(
          this.getValue(this.selectedZoomResolution),
          [Validators.required]
        ),
        fieldZoomSpectralAveraging: new UntypedFormControl(
          this.getValue(this.defaultZoomSpectralAveraging)
        ),
        fieldEffectiveResolution: new UntypedFormControl(null),
        fieldSupplied: new UntypedFormControl(
          this.getValue(this.defaultSupplied),
          [Validators.required]
        ),
        fieldIntegrationTime: new UntypedFormControl(
          this.getDefaultValue(this.fieldIntegrationTime),
          [Validators.required, this.validatorService.validatorGeneral()]
        ),
        fieldSensitivity: new UntypedFormControl(
          this.getDefaultValue(this.fieldSensitivity),
          [Validators.required, this.validatorService.validatorGeneral()]
        ),
        fieldImageWeighting: new UntypedFormControl(
          this.getValue(this.defaultSelectedImageWeighting)
        ),
        fieldRobust: new UntypedFormControl(0),
        fieldTapering: new UntypedFormControl(0, [Validators.required])
      },
      {
        validators: [
          this.validatorService.midZoomBandwidthValidator(
            this.selectedZoomFrequency,
            this._observingMode,
            this._subarrayType
          )
        ]
      }
    );
  }

  selectZoomFrequency(
    selectedFrequency: ConfigurationDropDownValue
  ) {
    this.selectedZoomFrequency = selectedFrequency;
    this.updateFrequencyAndBandwidthValidity();
  }

  getErrorMessage(fieldName: string) {
    const control: AbstractControl | null = this.lineForm.get(fieldName);
    return this.validatorService.getErrorMessage(control);
  }

  suppliedChangedZoomPanel(value: any): void {
    this.output?.suppliedChangedZoomResult(value);

    if (value === 'IntegrationTime') {
      this.lineForm.get('fieldIntegrationTime')?.enable();
      this.lineForm.get('fieldSensitivity')?.disable();
      const defaultSens = this.configuration!.midCalcFormFields!.find(e => e.field === 'fieldSensitivity')!.defaultValue;
      this.lineForm.get('fieldSensitivity')?.setValue(defaultSens);
    } else {
      this.lineForm.get('fieldSensitivity')?.enable();
      this.lineForm.get('fieldIntegrationTime')?.disable();
      const defaultInt = this.configuration!.midCalcFormFields!.find(e => e.field === 'fieldIntegrationTime')!.defaultValue;
      this.lineForm.get('fieldIntegrationTime')?.setValue(defaultInt);
    }
  }

  weightingChanged(value: any): void {
    const showRobustOption = this.fieldImageWeighting.defaultValue.find(
      (e: any) => e.robust
    );
    this.showRobustness = showRobustOption?.value === value;
    this.getImageWeightingStyle();
  }

  selectIntegrationTime(selectedIntegrationTime: ConfigurationDropDownValue): void {
    this.paramChange();
    this.selectedIntegrationTime = selectedIntegrationTime;
    this.lineForm.get('fieldIntegrationTime')?.markAsTouched();
  }

  selectSensitivity(selectedSelected: ConfigurationDropDownValue): void {
    this.paramChange();
    this.selectedSensitivity = selectedSelected;
    this.lineForm.get('fieldSensitivity')?.markAsTouched();
  }

  getSupplied() {
    return this.lineForm.get('fieldSupplied')?.value;
  }

  resetZoomForm(): void {
    this.getformFieldsDataFromConfig();
    this.lineForm.reset(this.lineFormInitialValues);
    this.suppliedChangedZoomPanel(this.defaultSupplied.value);
    this.weightingChanged(this.defaultSelectedImageWeighting.value);
    this.lineForm.markAsUntouched();
    this.formResult = null;
    this.output?.resetZoomResult();
  }

  getZoomFrequency(): number {
    const zoomFreq = this.lineForm.get(`fieldZoomFrequency`)?.value;
    return this.selectedZoomFrequency?.multiplier && this.selectedZoomFrequency?.operator
        ? this.validatorService.getScaledValue(zoomFreq, this.selectedZoomFrequency.multiplier, this.selectedZoomFrequency.operator)
        : zoomFreq;
  }

  getZoomSpectralResolution(): number {
    const value = this.lineForm.get('fieldEffectiveResolution')?.value;
    if (value) {
      if (typeof value === 'string') {
        return +(value.split(' ')[0]) * 1000;
      } else if (typeof value === 'number') {
        return value * 1000;
      }
    }
    return 0;
  }

  getScaledValue(field: ConfigurationMidCalculatorFormField, scale: ConfigurationDropDownValue): number {
    return this.validatorService.getScaledValue(
      field,
      scale.multiplier!,
      scale.operator!);
  }

  getZoomFormData(): LineFormData {
    let sensitivityScaled: null | number = null;
    let integrationTimeScaled: null | number = null;
    this.lineForm.updateValueAndValidity();

    integrationTimeScaled = +this.lineForm.get('fieldIntegrationTime')?.value;
    if (
      this.selectedIntegrationTime?.multiplier &&
      this.selectedIntegrationTime?.operator
    ) {
      integrationTimeScaled = this.validatorService.getScaledValue(
        this.lineForm.get('fieldIntegrationTime')?.value,
        this.selectedIntegrationTime.multiplier,
        this.selectedIntegrationTime.operator
      );
    }

    sensitivityScaled = +this.lineForm.get('fieldSensitivity')?.value;
    if (
      this.lineForm.get('fieldSensitivity')?.value &&
      this.selectedSensitivity?.multiplier &&
      this.selectedSensitivity?.operator
    ) {
      sensitivityScaled = this.validatorService.getScaledValue(
        this.lineForm.get('fieldSensitivity')?.value,
        this.selectedSensitivity.multiplier,
        this.selectedSensitivity.operator
      );
    }

    const selectedBandwdith = this.defaultResolutionDropdownOptions
      .find((option) => option.value == this.lineForm.get("fieldZoomResolution")?.value);

    const formData: LineFormData = {
      selectedSensitivityUnit: this.selectedSensitivity.value,
      zoomFrequency: this.getZoomFrequency(),
      zoomBandwidth: selectedBandwdith!.bandwidth,
      spectralResolution: this.getZoomSpectralResolution(),
      weighting: this.lineForm.get('fieldImageWeighting')?.value,
      calculator_mode: 'line',
      taper: this.lineForm.get('fieldTapering')?.value
    };

    const briggsVal = this.configuration!.midCalcFormFields!.find((e: any) => e.field === 'fieldImageWeighting')!.defaultValue.find((f: any) => f.option === 'Briggs').value;
    if (formData.weighting === briggsVal) {
      formData.robustness = this.lineForm.get('fieldRobust')?.value;
    }

    if (this.lineForm.get('fieldIntegrationTime')?.enabled) {
      formData.integrationTime = integrationTimeScaled;
    } else if (this.lineForm.get('fieldSensitivity')?.enabled) {
      formData.sensitivity = sensitivityScaled;
    }
    return formData;
  }

  getFieldSupplied(inputData: any): string {
    if (!inputData.integration_time) {
      return this.lineForm.get('fieldSupplied')?.value;
    } else {
      return constants.supplied.integrationTime;
    }
  }

  loadingResults(show: boolean): void {
    this.output?.loadingResults(show);
  }

  getResolutionDisplayValue(resolution: any, bandwidth: number, displayValue: string): string {
    const centralFreq = this.validatorService.getScaledValue
      (
        this.lineForm.get(`fieldZoomFrequency`)!.value,
        this.selectedZoomFrequency.multiplier!,
        this.selectedZoomFrequency.operator!
      );

    const velocity = '(' + helpers.calculate.calculateVelocity(resolution * 1000, centralFreq) + ')';
    const bandwidthVelocity = '(' + helpers.calculate.calculateVelocity(bandwidth, centralFreq) + ')';
    return displayValue.replace('%s', bandwidthVelocity).replace('%s', velocity);
  }

  setEffectiveResolution() {
    const centralFreq = this.validatorService.getScaledValue(
      this.lineForm.get("fieldZoomFrequency")!.value,
      this.selectedZoomFrequency.multiplier!,
      this.selectedZoomFrequency.operator!
    );
    const spectralAverage = this.lineForm.get("fieldZoomSpectralAveraging")?.value;
    const resolution = this.lineForm.get("fieldZoomResolution")?.value;
    const velocity = '(' + helpers.calculate.calculateVelocity(resolution * spectralAverage * 1000, centralFreq) + ')';
    this.lineForm.get("fieldEffectiveResolution")?.setValue(`${(spectralAverage * resolution)} kHz ${velocity}`);
  }

  resolutionChanged(resolution: number) {
    const centralFreq = this.validatorService.getScaledValue(
      this.lineForm.get("fieldZoomFrequency")!.value,
      this.selectedZoomFrequency.multiplier!,
      this.selectedZoomFrequency.operator!
    );
    this.updateBandwidthValidity();
    const spectralAverage = this.lineForm.get("fieldZoomSpectralAveraging")?.value;
    const velocity = '(' + helpers.calculate.calculateVelocity(resolution * spectralAverage * 1000, centralFreq) + ')';
    this.lineForm.get("fieldEffectiveResolution")?.setValue(`${(resolution * spectralAverage)} kHz ${velocity}`);
  }

  spectralAveragingChanged(spectralAverage: any) {
    const centralFreq = this.validatorService.getScaledValue(
      this.lineForm.get("fieldZoomFrequency")!.value,
      this.selectedZoomFrequency.multiplier!,
      this.selectedZoomFrequency.operator!
    );
    const resolution = this.lineForm.get("fieldZoomResolution")?.value;
    const velocity = '(' + helpers.calculate.calculateVelocity(resolution * spectralAverage * 1000, centralFreq) + ')';
    this.lineForm.get("fieldEffectiveResolution")?.setValue(`${(spectralAverage * resolution)} kHz ${velocity}`);
  }

  getScaledFrequency() {
    if (this.lineForm && this.selectedZoomFrequency) {
      const scaledFreq = this.validatorService.getScaledValue(this.lineForm.get('fieldZoomFrequency')!.value, this.selectedZoomFrequency.multiplier!, this.selectedZoomFrequency.operator!);
      return scaledFreq / 1000000000;
    }
    return 0;
  }

  disableWeightingFields() {
    // In custom array mode, weighting should be set to natural and disabled as we can't calculate
    // the weighting factors, there should be no tapering, and sensitivities should only be available in Jy/beam.
    this.lineForm.get('fieldImageWeighting')?.setValue('natural');
    this.weightingChanged('natural');
    this.lineForm.get('fieldImageWeighting')?.disable();
    this.lineForm.get('fieldTapering')?.setValue(0);
    this.lineForm.get('fieldTapering')?.disable();
    this.defaultSensitivityDropdownOptions = this.configuration!.dropDownValues!
      .filter((e: ConfigurationDropDownValue) => e.type === 'sensitivity' && !e.value.includes('K'))
      .sort(this.unitMagnitudeCompareFn);
    // Set the selected unit back to Jy/beam, incase it was in K before the custom array was selected
    this.selectedSensitivity = this.defaultSensitivityDropdownOptions.find((e: ConfigurationDropDownValue) => e.selected)!;
  }

  enableWeightingFields() {
    this.lineForm.get('fieldImageWeighting')?.enable();
    this.lineForm.get('fieldImageWeighting')?.enable();
    this.lineForm.get('fieldTapering')?.enable();
    this.setDefaultSensitivityDropdownOptions();
    // Call weightingChanged to ensure the robustness and style is correct
    this.weightingChanged(this.lineForm.get('fieldImageWeighting')?.value);
  }
}
