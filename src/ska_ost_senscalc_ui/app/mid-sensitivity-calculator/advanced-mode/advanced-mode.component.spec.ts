import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from "@angular/material/card";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import {MatSlideToggleChange, MatSlideToggleModule} from "@angular/material/slide-toggle";

import * as config from '../../../assets/configuration.json';
import { AdvancedModeComponent } from './advanced-mode.component';

describe('AdvancedModeComponent', () => {
  let component: AdvancedModeComponent;
  let fixture: ComponentFixture<AdvancedModeComponent>;

  const falseToggleCheckEvent = {
    checked: false
  } as MatSlideToggleChange;

  const trueToggleCheckEvent = {
    checked: true
  } as MatSlideToggleChange;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AdvancedModeComponent
      ],
      imports: [
        NoopAnimationsModule,
        FormsModule,
        HttpClientModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatSlideToggleModule,
        ReactiveFormsModule
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedModeComponent);
    component = fixture.componentInstance;
    component.configuration = config;
    component.selectedsubarrayType = {
      name: 'foo',
      label: 'bar',
      n_ska: 133,
      n_meer: 64
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle the eta fields correctly', () => {
    component.toggleEtaFields(trueToggleCheckEvent, 'fieldEtaOther');
    expect(component.toggleEtaOtherEnabled).toBeTruthy();
    expect(component.toggleEtaSystemEnabled).toBeFalsy();

    component.toggleEtaFields(trueToggleCheckEvent, 'fieldEtaSystem');
    expect(component.toggleEtaSystemEnabled).toBeTruthy();
    expect(component.toggleEtaOtherEnabled).toBeFalsy();

    component.toggleEtaFields(falseToggleCheckEvent, 'fieldEtaSystem');
    expect(component.toggleEtaSystemEnabled).toBeFalsy();

    component.toggleEtaFields(falseToggleCheckEvent, 'fieldEtaOther');
    expect(component.toggleEtaOtherEnabled).toBeFalsy();
  });

  it('toggle Temp Fields', () => {
    component.toggleTempFields(trueToggleCheckEvent, 'fieldTsysSKA', 'SKA');
    expect(component.toggleTsysSKAEnabled).toBeTruthy();
    expect(component.toggleTrcvSKAEnabled).toBeFalsy();
    expect(component.advancedModeForm.get('fieldTsysSKA')!.enabled).toBeTruthy();
    component.toggleTempFields(trueToggleCheckEvent, 'fieldTsysMeerKAT', 'MeerKAT');
    expect(component.toggleTsysMeerEnabled).toBeTruthy();
    expect(component.toggleTrcvMeerEnabled).toBeFalsy();
    expect(component.advancedModeForm.get('fieldTsysMeerKAT')!.enabled).toBeTruthy();
    component.toggleTempFields(falseToggleCheckEvent, 'fieldTsysSKA', 'SKA');
    expect(component.toggleTsysSKAEnabled).toBeFalsy();
    expect(component.toggleTrcvSKAEnabled).toBeFalsy();
    expect(component.advancedModeForm.get('fieldTsysSKA')!.disabled).toBeTruthy();
    component.toggleTempFields(falseToggleCheckEvent, 'fieldTsysMeerKAT', 'MeerKAT');
    expect(component.toggleTsysMeerEnabled).toBeFalsy();
    expect(component.toggleTrcvMeerEnabled).toBeFalsy();
    expect(component.advancedModeForm.get('fieldTsysMeerKAT')!.disabled).toBeTruthy();
    component.toggleTempFields(trueToggleCheckEvent, 'fieldTrcvSKA', 'SKA');
    expect(component.toggleTsysSKAEnabled).toBeFalsy();
    expect(component.toggleTrcvSKAEnabled).toBeTruthy();
    expect(component.advancedModeForm.get('fieldTrcvSKA')!.enabled).toBeTruthy();
    component.toggleTempFields(trueToggleCheckEvent, 'fieldTrcvMeerKAT', 'MeerKAT');
    expect(component.toggleTsysMeerEnabled).toBeFalsy();
    expect(component.toggleTrcvMeerEnabled).toBeTruthy();
    expect(component.advancedModeForm.get('fieldTrcvMeerKAT')!.enabled).toBeTruthy();
    component.toggleTempFields(falseToggleCheckEvent, 'fieldTrcvSKA', 'SKA');
    expect(component.toggleTsysSKAEnabled).toBeFalsy();
    expect(component.toggleTrcvSKAEnabled).toBeFalsy();
    expect(component.advancedModeForm.get('fieldTrcvSKA')!.disabled).toBeTruthy();
    component.toggleTempFields(falseToggleCheckEvent, 'fieldTrcvMeerKAT', 'MeerKAT');
    expect(component.toggleTsysMeerEnabled).toBeFalsy();
    expect(component.toggleTrcvMeerEnabled).toBeFalsy();
    expect(component.advancedModeForm.get('fieldTrcvMeerKAT')!.disabled).toBeTruthy();
  });

  it('toggle Sky Fields', () => {
    component.toggleSkyFields(trueToggleCheckEvent, 'fieldTsky');
    expect(component.toggleTskyEnabled).toBeTruthy();
    expect(component.toggleTalphaEnabled).toBeFalsy();
    expect(component.toggleTgalEnabled).toBeFalsy();
    expect(component.advancedModeForm.get('fieldTsky')!.enabled).toBeTruthy();

    component.toggleSkyFields(trueToggleCheckEvent, 'fieldTgal');
    expect(component.toggleTskyEnabled).toBeFalsy();
    expect(component.toggleTalphaEnabled).toBeFalsy();
    expect(component.toggleTgalEnabled).toBeTruthy();
    expect(component.advancedModeForm.get('fieldTgal')!.enabled).toBeTruthy();

    component.toggleSkyFields(trueToggleCheckEvent, 'fieldTalpha');
    expect(component.toggleTskyEnabled).toBeFalsy();
    expect(component.toggleTalphaEnabled).toBeTruthy();
    expect(component.toggleTgalEnabled).toBeFalsy();
    expect(component.advancedModeForm.get('fieldTalpha')!.enabled).toBeTruthy();

    component.toggleSkyFields(falseToggleCheckEvent, 'fieldTalpha');
    expect(component.advancedModeForm.get('fieldTalpha')!.disabled).toBeTruthy();
  });

  it('get advanced Form data', () => {
    let data = component.getAdvancedFormData();
    expect(data?.etaPointing).toBe(null);
    component.advancedModeForm.get('fieldEtaPointing')!.setValue(0.1);
    component.advancedModeForm.updateValueAndValidity();
    data = component.getAdvancedFormData();
    expect(data?.etaPointing).toBe(0.1);
  });
});
