import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MidCalculatorComponent } from './mid-calculator/mid-calculator.component';

const routes: Routes = [
  {
    path: '',
    component: MidCalculatorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MidSensitivityCalculatorRoutingModule {
}
