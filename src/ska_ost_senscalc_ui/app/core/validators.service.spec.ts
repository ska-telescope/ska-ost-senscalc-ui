import { TestBed } from '@angular/core/testing';

import { ValidatorsService } from './validators.service';

import { AbstractControl, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, FormsModule, ReactiveFormsModule, ValidationErrors, Validators } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {ArrayDropDownValue, ConfigurationDropDownValue} from '../app-configuration';
import * as config from '../../assets/configuration.json';
import {LowSubarraysResponse} from "../generated/api/low";

describe('ValidatorsService', () => {
  let service: ValidatorsService;
  let isFreqContainedMock: any;
  let isBandwidthContainedMock: any;
  const mixedSubarray: ArrayDropDownValue = {
    name: 'foo',
    label: 'foo',
    n_ska: 133,
    n_meer: 64
  };
  const skaSubarray: ArrayDropDownValue = {
    name: 'foo',
    label: 'foo',
    n_ska: 133,
    n_meer: 0
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, FormsModule,
        ReactiveFormsModule]
    });
    service = TestBed.inject(ValidatorsService);
    isFreqContainedMock = jest.spyOn(service, "isFreqContained");
    isBandwidthContainedMock = jest.spyOn(service, 'isBandwidthContained');
    service.setConfig(config);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('test validatorRightAscension', () => {
    const raField = 'RightAscension';
    it.each(["24:00:00", "-10:00:00.00", "20:80:00.00", "01:23:456.0"])('test RA %p is invalid', (invalidRA: string) => {
      const formGroup = new UntypedFormGroup({
        [raField]: new UntypedFormControl(null, [service.validatorRightAscension(false)])
      });
      formGroup.patchValue({
        [raField]: invalidRA
      });
      expect(formGroup.get(raField)?.valid).toBe(false);
      expect(formGroup.get(raField)?.hasError('errorMessage')).toBe(true);

      formGroup.patchValue({
        [raField]: "20:00:00"
      });

      // Check that test works properly and errors are cleared on a valid value
      expect(formGroup.get(raField)?.valid).toBe(true);
      expect(formGroup.get(raField)?.hasError('errorMessage')).toBe(false);
    });

    it.each(["01:23:45.", "00:00:00.00", "23:59:59.9999", "13:25:27.60"])('test RA %p is valid', (validInput: string) => {
      const formGroup = new UntypedFormGroup({
        [raField]: new UntypedFormControl(null, [service.validatorRightAscension(false)])
      });
      formGroup.patchValue({
        [raField]: validInput
      });

      expect(formGroup.get(raField)?.valid).toBe(true);
      expect(formGroup.get(raField)?.hasError('errorMessage')).toBe(false);
    });
  });

  describe('test validatorDeclination', () => {
    const decField = 'Declination';
    it.each([["000:0.99", "Low"], ["40:60:00", "Mid"], ["45:00:60.00", "Low"], ["45:00:00", "Mid"], ["50:00:00.00", "Low"], ["-90:00:30.00", "Mid"], ["-90:00:00.99", "Low"], ["-100:00:00.00", "Mid"], ["-89:00:122", "Mid"]])
    ('test declination %p for telescope %p is invalid', (invalidDec: string, telescope: string) => {
      const formGroup = new UntypedFormGroup({
        [decField]: new UntypedFormControl(null, [service.validatorDeclination(telescope, false)])
      });
      formGroup.patchValue({
        [decField]: invalidDec
      });
      expect(formGroup.get(decField)?.valid).toBe(false);
      expect(formGroup.get(decField)?.hasError('errorMessage')).toBe(true);

      formGroup.patchValue({
        [decField]: "20:00:00"
      });

      // Check that test works properly and errors are cleared on a valid value
      expect(formGroup.get(decField)?.valid).toBe(true);
      expect(formGroup.get(decField)?.hasError('errorMessage')).toBe(false);
    });

    it.each([["00:00:00", "Mid"], ["00:00:00", "Low"], ["40:00:00", "Mid"], ["45:00:00.00", "Low"], ["-90:00:00.00", "Mid"], ["-90:00:00.00", "Low"]])
    ('test declination %p for telescope %p is valid', (validDec: string, telescope: string) => {
      const formGroup = new UntypedFormGroup({
        [decField]: new UntypedFormControl(null, [service.validatorDeclination(telescope, false)])
      });
      formGroup.patchValue({
        [decField]: validDec
      });

      expect(formGroup.get(decField)?.valid).toBe(true);
      expect(formGroup.get(decField)?.hasError('errorMessage')).toBe(false);
    });
  });

  describe('test validatorMinReceiverNum', () => {
    const skaField = 'nSKA';
    const meerkatField = "nMeerKAT";
    it.each([[0, 0], [0, 1], [1, 0]])('test validatorMinReceiverNum returns error when given %p SKA and %p MeerKAT antennas', (nSKA, nMeer) => {
      const formGroup = new UntypedFormGroup({
        [skaField]: new UntypedFormControl(1, [service.validatorMinReceiverNum(skaField, meerkatField)]),
        [meerkatField]: new UntypedFormControl(1, [service.validatorMinReceiverNum(skaField, meerkatField)])
      });
      expect(formGroup.get(skaField)?.valid).toBe(true);
      expect(formGroup.get(meerkatField)?.valid).toBe(true);
      formGroup.patchValue({
        [skaField]: nSKA,
        [meerkatField]: nMeer
      });
      formGroup.get(skaField)?.markAsTouched();
      formGroup.get(skaField)?.updateValueAndValidity();
      formGroup.get(meerkatField)?.markAsTouched();
      formGroup.get(meerkatField)?.updateValueAndValidity();
      expect(formGroup.get(skaField)?.valid).toBe(false);
      expect(formGroup.get(meerkatField)?.valid).toBe(false);
      expect(formGroup.get(skaField)?.hasError('errorMessage')).toBe(true);
      expect(formGroup.get(meerkatField)?.hasError('errorMessage')).toBe(true);
    });

    it.each([[2, 0], [0, 2], [1, 1]])('test validatorMinReceiverNum returns no errors when given %p SKA and %p MeerKAT antennas', (nSKA, nMeer) => {
      const formGroup = new UntypedFormGroup({
        [skaField]: new UntypedFormControl(null, [service.validatorMinReceiverNum(skaField, meerkatField)]),
        [meerkatField]: new UntypedFormControl(null, [service.validatorMinReceiverNum(skaField, meerkatField)])
      });
      formGroup.patchValue({
        [skaField]: nSKA,
        [meerkatField]: nMeer
      });
      formGroup.get(skaField)?.markAsTouched();
      formGroup.get(skaField)?.updateValueAndValidity();
      formGroup.get(meerkatField)?.markAsTouched();
      formGroup.get(meerkatField)?.updateValueAndValidity();
      expect(formGroup.get(skaField)?.valid).toBe(true);
      expect(formGroup.get(meerkatField)?.valid).toBe(true);
      expect(formGroup.get(skaField)?.hasError('errorMessage')).toBe(false);
      expect(formGroup.get(meerkatField)?.hasError('errorMessage')).toBe(false);
    });
  });

  it('test validatorInteger', () => {
    const control: AbstractControl = new UntypedFormControl('testControl', [service.validatorInteger()]);
    const fb: UntypedFormBuilder = new UntypedFormBuilder();
    const form: UntypedFormGroup = fb.group({
      'testControl': control
    });

    form.get('testControl')?.setValue('Test');
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe('Test');

    let errors: ValidationErrors | null | undefined = form.get('testControl')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('errorMessage');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('testControl')?.setValue(-1);
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe(-1);

    errors = form.get('testControl')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('errorMessage');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('testControl')?.setValue(1.1);
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe(1.1);

    errors = form.get('testControl')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('errorMessage');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('testControl')?.setValue(1);
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe(1);
    errors = form.get('testControl')?.errors;
    if (errors) {
      throw new Error('expected no error');
    }

    form.get('testControl')?.setValue(null);
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe(null);
    errors = form.get('testControl')?.errors;
    if (errors) {
      throw new Error('expected no error');
    }
  });

  it('test validatorEfficiency', () => {
    const efficiency = 'efficiencyControl';
    const formGroup = new UntypedFormGroup({
      [efficiency]: new UntypedFormControl(null, [service.validatorEfficiency()])
    });

    formGroup.patchValue({
      [efficiency]: '0'
    });

    expect(formGroup.get(efficiency)?.hasError('errorMessage')).toBe(true);

    formGroup.patchValue({
      [efficiency]: '1'
    });

    expect(formGroup.get(efficiency)?.valid).toBe(true);
    expect(formGroup.get(efficiency)?.hasError('errorMessage')).toBe(false);

    formGroup.patchValue({
      [efficiency]: 'Test'
    });

    expect(formGroup.get(efficiency)?.hasError('errorMessage')).toBe(true);
  });

  describe('test validatorElevation', () => {
    const decField = 'declination';
    const elField = 'elevation';
    it.each([[19.26, "40:00:00"], [18, "44:00:00"], [14, "40:00:00"], [0, "40:00:00"]])('test elevation %p is invalid for declination %p', (invalidElevation: number, dec:string) => {
      const formGroup = new UntypedFormGroup({
        [decField]: new UntypedFormControl(dec),
        [elField]: new UntypedFormControl(null, [service.validatorElevationMid(15, decField)])
      });
      formGroup.patchValue({
        [elField]: invalidElevation
      });
      expect(formGroup.get(elField)?.valid).toBe(false);
      expect(formGroup.get(elField)?.hasError('errorMessage')).toBe(true);

      formGroup.patchValue({
        [elField]: 15
      });

      // Check that test works properly and errors are cleared on a valid value
      expect(formGroup.get(elField)?.valid).toBe(true);
      expect(formGroup.get(elField)?.hasError('errorMessage')).toBe(false);
    });

    it.each([[19.19, "40:00:00"], [15.2, "44:00:00"], [15, "40:00:00"]])('test elevation %p is valid for declination %p', (validInput, dec) => {
      const formGroup = new UntypedFormGroup({
        [decField]: new UntypedFormControl(dec),
        [elField]: new UntypedFormControl(null, [service.validatorElevationMid(15, decField)])
      });
      formGroup.patchValue({
        [elField]: validInput
      });

      expect(formGroup.get(elField)?.valid).toBe(true);
      expect(formGroup.get(elField)?.hasError('errorMessage')).toBe(false);
    });
  });

  describe('test validatorNumberMinMax', () => {
    const formControl = 'testControl';
    it.each(['Test', -1, 0, 3.1])('test %p is invalid for range 1 to 3', (invalidInput) => {
      const formGroup = new UntypedFormGroup({
        [formControl]: new UntypedFormControl(null, [service.validatorNumberMinMax(1, 3)])
      });
      formGroup.patchValue({
        [formControl]: invalidInput
      });
      expect(formGroup.get(formControl)?.valid).toBe(false);
      expect(formGroup.get(formControl)?.hasError('errorMessage')).toBe(true);

      formGroup.patchValue({
        [formControl]: '1'
      });

      expect(formGroup.get(formControl)?.valid).toBe(true);
      expect(formGroup.get(formControl)?.hasError('errorMessage')).toBe(false);
    });

    it.each([0, 1, 2.5, 3])('test %p is valid for range 0 to 3', (validInput) => {
      const formGroup = new UntypedFormGroup({
        [formControl]: new UntypedFormControl(null, [service.validatorNumberMinMax(0, 3)])
      });
      formGroup.patchValue({
        [formControl]: validInput
      });

      expect(formGroup.get(formControl)?.valid).toBe(true);
      expect(formGroup.get(formControl)?.hasError('errorMessage')).toBe(false);
    });
  });

  it('test validatorGeneral', () => {
    const control: AbstractControl = new UntypedFormControl('testControl', [service.validatorGeneral()]);
    const fb: UntypedFormBuilder = new UntypedFormBuilder();
    const form: UntypedFormGroup = fb.group({
      'testControl': control
    });

    form.get('testControl')?.setValue('Test');
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe('Test');

    let errors: ValidationErrors | null | undefined = form.get('testControl')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('errorMessage');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('testControl')?.setValue(-1);
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe(-1);

    errors = form.get('testControl')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('errorMessage');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('testControl')?.setValue("0");
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe("0");

    errors = form.get('testControl')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('errorMessage');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('testControl')?.setValue(2);
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe(2);
    errors = form.get('testControl')?.errors;
    if (errors) {
      throw new Error('expected no error');
    }
  });

  it('test centralFrequencyValidator', () => {
    const freqScale: ConfigurationDropDownValue = {
      type: 'frequency',
      value: 'GHz',
      operator: '*',
      multiplier: 1000000000
    };
    const control: AbstractControl = new UntypedFormControl({ value: 0.7975 }, [Validators.required, service.centralFrequencyValidator(freqScale, 'Band 1', mixedSubarray)]);
    const fb: UntypedFormBuilder = new UntypedFormBuilder();
    const form: UntypedFormGroup = fb.group({
      'testControl': control
    });

    form.get('testControl')?.setValue('Test');
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe('Test');

    let errors: ValidationErrors | null | undefined = form.get('testControl')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('numericError');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('testControl')?.setValue('');
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe('');

    errors = form.get('testControl')?.errors;

    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('required');
    } else {
      throw new Error('expected error in validation');
    }

    isFreqContainedMock.mockImplementation(() => false);

    form.get('testControl')?.setValue(3.5);
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe(3.5);
    errors = form.get('testControl')?.errors;

    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('rangeError');
    } else {
      throw new Error('expected error in validation');
    }

    isFreqContainedMock.mockImplementation(() => true);

    form.get('testControl')?.setValue(6.5);
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe(6.5);
    errors = form.get('testControl')?.errors;

    if (errors) {
      throw new Error('expected error in validation');
    }

  });

  describe('Test low bandwidth validation functions', () => {
    const freqField = "centralFrequency";
    const bwField = "bandwidth";
    describe('test lowContinuumBandwidthValidator', () => {
      it.each([
        ["numericError", "Test", 60],
        ["bandwidthRangeError", 301, 200],
        ["bandwidthRangeError", 11, 55],
        ["minimumChannelWidthError", 0.005, 60]
      ])(
        'test lowContinuumBandwidthValidator returns %p error for bandwidth %p and frequency %p',
        (errorKey: string, invalidValue: string | number, frequency: number) => {
        const formGroup = new UntypedFormGroup({
            [freqField]: new UntypedFormControl(frequency),
            [bwField]: new UntypedFormControl(null, [service.lowContinuumBandwidthValidator()])
          }
        );
        formGroup.patchValue({
          [bwField]: invalidValue
        });
        expect(formGroup.get(bwField)?.valid).toBe(false);
        expect(formGroup.get(bwField)?.hasError(errorKey)).toBe(true);

        formGroup.patchValue({
          [bwField]: 5
        });
        formGroup.updateValueAndValidity();

        // Check that test works properly and errors are cleared on a valid value
        expect(formGroup.get(bwField)?.valid).toBe(true);
        expect(formGroup.get(bwField)?.errors).toBe(null);
      });

      it.each([
        [300, 200],
        [10, 55]
      ])(
        'test lowContinuumBandwidthValidator returns no errors for bandwidth %p and frequency %p',
        (validInput: number, frequency: number) => {
        const formGroup = new UntypedFormGroup({
            [freqField]: new UntypedFormControl(frequency),
            [bwField]: new UntypedFormControl(null, [service.lowContinuumBandwidthValidator()])
          }
        );
        formGroup.patchValue({
          [bwField]: validInput
        });

        expect(formGroup.get(bwField)?.valid).toBe(true);
        expect(formGroup.get(bwField)?.hasError('errorMessage')).toBe(false);
      });
    });

    describe('test lowZoomBandwidthValidator', () => {
      const validValue = {
        spectralResolutionHz: 14.1,
        totalBandwidthKhz: 24.4
      };
      it.each([
        ["bandwidthRangeError", 3125, 349],
        ["bandwidthRangeError", 3125, 51]
      ])(
        'test lowZoomBandwidthValidator returns %p error for bandwidth %p kHz and frequency %p MHz',
        (errorKey: string, invalidValue: number, frequency: number) => {
        const formGroup = new UntypedFormGroup({
            [freqField]: new UntypedFormControl(frequency),
            [bwField]: new UntypedFormControl(validValue, [service.lowZoomBandwidthValidator()])
          }
        );
        formGroup.patchValue({
          [bwField]: {
            spectralResolutionHz: invalidValue / (1728 * 1e-3),
            totalBandwidthKhz: invalidValue
          }
        });
        expect(formGroup.get(bwField)?.valid).toBe(false);
        expect(formGroup.get(bwField)?.hasError(errorKey)).toBe(true);

        formGroup.patchValue({
          [bwField]: validValue
        });
        formGroup.updateValueAndValidity();

        // Check that test works properly and errors are cleared on a valid value
        expect(formGroup.get(bwField)?.valid).toBe(true);
        expect(formGroup.get(bwField)?.errors).toBe(null);
      });

      it.each([
        [1562.5, 349],
        [1562.5, 51]
      ])(
        'test lowZoomBandwidthValidator returns no errors for bandwidth %p kHz and frequency %p MHz',
        (validInput: number, frequency: number) => {
        const formGroup = new UntypedFormGroup({
            [freqField]: new UntypedFormControl(frequency),
            [bwField]: new UntypedFormControl(validValue, [service.lowZoomBandwidthValidator()])
          }
        );
        formGroup.patchValue({
          [bwField]: {
            spectralResolutionHz: validInput / (1728 * 1e-3),
            totalBandwidthKhz: validInput
          }
        });

        expect(formGroup.get(bwField)?.valid).toBe(true);
        expect(formGroup.get(bwField)?.hasError('errorMessage')).toBe(false);
      });

      it.each([
        ["contBandwidthMaximumExceeded", 100*1e3, {name: 'LOW_AA05_all', label: 'AA0.5', n_stations: 200}],
        ["contBandwidthMaximumExceeded", 100*1e3, {name: 'LOW_AA1_all', label: 'AA1', n_stations: 200}],
        ["contBandwidthMaximumExceeded", 160*1e3, {name: 'LOW_AA2_all', label: 'AA02', n_stations: 200}]
      ])(
        'test lowZoomBandwidthValidator returns %p error for bandwidth %p kHz and subarray %p',
        (errorKey: string, invalidBandwidthKhz: number, subarray: LowSubarraysResponse) => {
        const formGroup = new UntypedFormGroup({
            [freqField]: new UntypedFormControl(200),
            [bwField]: new UntypedFormControl({
            totalBandwidthKhz: invalidBandwidthKhz
          }, [service.lowZoomBandwidthValidator(subarray)])
          }
        );

        expect(formGroup.get(bwField)?.valid).toBe(false);
        expect(formGroup.get(bwField)?.hasError(errorKey)).toBe(true);
      });
    });
  });

  describe('Test mid bandwidth validation functions', () => {
    const freqScale = {
      "type": "frequency",
      "value": "GHz",
      "operator": "*",
      "multiplier": 1000000000,
      "selected": true
    };
    const SKASub: ArrayDropDownValue = {
      "name": "AA4_SKA_only",
      "label": "AA4 (15 m antennas only)",
      "n_ska": 10,
      "n_meer": 0
    };
    const MeerKATSub: ArrayDropDownValue = {
      "name": "AA4_MeerKAT_only",
      "label": "AA4 (13.5 m antennas only)",
      "n_ska": 0,
      "n_meer": 10
    };
    const observingMode = 'Band 1';
    describe('test midContinuumBandwidthValidator', () => {
      const freqField = 'fieldCentralFrequency';
      const bwField = 'fieldBandwidth';
      it.each([
        ["numericError", "Test", 0.7, SKASub],
        ["bandwidthRangeError", 0.2, 1.0, SKASub],
        ["bandwidthRangeError", 0.1, 0.39, SKASub],
        ["bandwidthRangeError", 0.1, 1.0, MeerKATSub],
        ["bandwidthRangeError", 0.05, 0.59, MeerKATSub],
        ["minimumChannelWidthError", 0.000001, 0.7, MeerKATSub]
      ])(
        'test midContinuumBandwidthValidator returns %p error for bandwidth %p GHz, frequency %p GHz and sub-array %p',
        (errorKey: string, invalidValue: string | number, frequency: number, subarray: ArrayDropDownValue) => {
        const formGroup = new UntypedFormGroup({
            [freqField]: new UntypedFormControl(frequency),
            [bwField]: new UntypedFormControl(null)
          },
          {
            validators: [service.midContinuumBandwidthValidator(freqScale, freqScale, observingMode, subarray)]
          }
        );
        formGroup.patchValue({
          [bwField]: invalidValue
        });
        expect(formGroup.get(bwField)?.valid).toBe(false);
        expect(formGroup.get(bwField)?.hasError(errorKey)).toBe(true);

        formGroup.patchValue({
          [bwField]: 0.005
        });
        formGroup.updateValueAndValidity();

        // Check that test works properly and errors are cleared on a valid value
        expect(formGroup.get(bwField)?.valid).toBe(true);
        expect(formGroup.get(bwField)?.errors).toBe(null);
      });

      it.each([
        [0.1, 1.0, SKASub],
        [0.05, 0.39, SKASub],
        [0.025, 1.0, MeerKATSub],
        [0.0125, 0.59, MeerKATSub]
      ])(
        'test midZoomBandwidthValidator returns no errors for resolution %p, frequency %p and sub-array %p',
        (validInput: number, frequency: number, subarray: ArrayDropDownValue) => {
        const formGroup = new UntypedFormGroup({
            [freqField]: new UntypedFormControl(frequency),
            [bwField]: new UntypedFormControl(null)
          },
          {
            validators: [service.midContinuumBandwidthValidator(freqScale, freqScale, observingMode, subarray)]
          }
        );
        formGroup.patchValue({
          [bwField]: validInput
        });

        expect(formGroup.get(bwField)?.valid).toBe(true);
        expect(formGroup.get(bwField)?.hasError('errorMessage')).toBe(false);
      });
    });

    describe('test midZoomBandwidthValidator', () => {
      const freqField = 'fieldZoomFrequency';
      const resField = 'fieldZoomResolution';
      it.each([
        ["numericError", "Test", 0.7, SKASub],
        ["bandwidthRangeError", 13.44, 1.0, SKASub],
        ["bandwidthRangeError", 6.72, 0.39, SKASub],
        ["bandwidthRangeError", 6.72, 1.0, MeerKATSub],
        ["bandwidthRangeError", 3.36, 0.59, MeerKATSub]
      ])(
        'test midZoomBandwidthValidator returns %p error for resolution %p, frequency %p and sub-array %p',
        (errorKey: string, invalidValue: string | number, frequency: number, subarray: ArrayDropDownValue) => {
        const formGroup = new UntypedFormGroup({
            [freqField]: new UntypedFormControl(frequency),
            [resField]: new UntypedFormControl(null)
          },
          {
            validators: [service.midZoomBandwidthValidator(freqScale, observingMode, subarray)]
          }
        );
        formGroup.patchValue({
          [resField]: invalidValue
        });
        expect(formGroup.get(resField)?.valid).toBe(false);
        expect(formGroup.get(resField)?.hasError(errorKey)).toBe(true);

        formGroup.patchValue({
          [resField]: 0.21
        });
        formGroup.updateValueAndValidity();

        // Check that test works properly and errors are cleared on a valid value
        expect(formGroup.get(resField)?.valid).toBe(true);
        expect(formGroup.get(resField)?.errors).toBe(null);
      });

      it.each([
        [6.72, 1.0, SKASub],
        [3.36, 0.39, SKASub],
        [1.68, 1.0, MeerKATSub],
        [0.84, 0.59, MeerKATSub]
      ])(
        'test midZoomBandwidthValidator returns no errors for resolution %p, frequency %p and sub-array %p',
        (validInput: number, frequency: number, subarray: ArrayDropDownValue) => {
        const formGroup = new UntypedFormGroup({
            [freqField]: new UntypedFormControl(frequency),
            [resField]: new UntypedFormControl(null)
          },
          {
            validators: [service.midZoomBandwidthValidator(freqScale, observingMode, subarray)]
          }
        );
        formGroup.patchValue({
          [resField]: validInput
        });

        expect(formGroup.get(resField)?.valid).toBe(true);
        expect(formGroup.get(resField)?.hasError('errorMessage')).toBe(false);
      });
    });
  });

  it('getBandLimits', () => {
    expect(service.getBandLimits('Band 2', mixedSubarray)).toStrictEqual([950000000, 1670000000]);
    expect(service.getBandLimits('Band 1', mixedSubarray)).toStrictEqual([580000000, 1015000000]);
  });

  it('getScaledValue', () => {
    expect(service.getScaledValue(6.5, 1000, '*')).toBe(6500);
    expect(service.getScaledValue(65000, 1000, '/')).toBe(65);
    expect(service.getScaledValue(6.5, 100, '')).toBe(6.5);
  });

  it('isFrequencyContained', () => {
    const res = service.isFreqContained('6500000000', 'Band 5a', skaSubarray);
    expect(res).toBe(true);
  });

  it('isBandwidthContained', () => {
    let result = service.isBandwidthContained(6500000000, 800000000, 'Band 5a', skaSubarray);
    expect(result).toBe(true);

    result = service.isBandwidthContained(6500000000, 4000000000, 'Band 5a', skaSubarray);
    expect(result).toBe(false);
  });

  it('isBandwidthContainedLow', () => {
    let result = service.isBandwidthContainedLow(200, 300);
    expect(result).toBe(true);

    result = service.isBandwidthContainedLow(200, 3000);
    expect(result).toBe(false);

    result = service.isBandwidthContainedLow(300, 100);
    expect(result).toBe(true);

    result = service.isBandwidthContainedLow(300, 120);
    expect(result).toBe(false);

    result = service.isBandwidthContainedLow(100, 100);
    expect(result).toBe(true);

    result = service.isBandwidthContainedLow(100, 3000);
    expect(result).toBe(false);
  });

  it('lowContinuumBandwidthValidator', () => {
    const control: AbstractControl = new UntypedFormControl(null, [Validators.required, service.lowContinuumBandwidthValidator()]);
    const freqControl: AbstractControl = new UntypedFormControl(null, [Validators.required]);
    const fb: UntypedFormBuilder = new UntypedFormBuilder();
    const form: UntypedFormGroup = fb.group({
      'continuumBandwidth': control,
      'centralFrequency': freqControl
    });

    form.get('continuumBandwidth')?.setValue('Test');
    form.get('centralFrequency')?.setValue('Test');
    form.updateValueAndValidity();

    expect(form.get('continuumBandwidth')?.value).toBe('Test');
    expect(form.get('centralFrequency')?.value).toBe('Test');

    let errors: ValidationErrors | null | undefined = form.get('continuumBandwidth')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('numericError');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('continuumBandwidth')?.setValue('');
    form.get('centralFrequency')?.setValue('');
    form.updateValueAndValidity();

    expect(form.get('continuumBandwidth')?.value).toBe('');

    errors = form.get('continuumBandwidth')?.errors;

    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('required');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('continuumBandwidth')?.setValue(-1);
    form.updateValueAndValidity();

    expect(form.get('continuumBandwidth')?.value).toBe(-1);

    errors = form.get('continuumBandwidth')?.errors;

    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('minimumChannelWidthError');
    } else {
      throw new Error('expected error in validation');
    }

    isBandwidthContainedMock.mockImplementation(() => false);
    form.get('continuumBandwidth')?.setValue(9);
    form.get('centralFrequency')?.setValue(6.5);
    form.updateValueAndValidity();

    expect(form.get('continuumBandwidth')?.value).toBe(9);
    expect(form.get('centralFrequency')?.value).toBe(6.5);
    errors = form.get('continuumBandwidth')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('bandwidthRangeError');
    } else {
      throw new Error('expected error in validation');
    }

  });

  it('minimumValue', () => {
    const control: AbstractControl = new UntypedFormControl('testControl', [service.minimumValue(2)]);
    const fb: UntypedFormBuilder = new UntypedFormBuilder();
    const form: UntypedFormGroup = fb.group({
      'testControl': control
    });

    form.get('testControl')?.setValue('Test');
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe('Test');

    let errors: ValidationErrors | null | undefined = form.get('testControl')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('errorMessage');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('testControl')?.setValue(2);
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe(2);
    errors = form.get('testControl')?.errors;
    if (errors) {
      throw new Error('expected error in validation');
    }

    form.get('testControl')?.setValue(1);
    form.updateValueAndValidity();

    expect(form.get('testControl')?.value).toBe(1);
    errors = form.get('testControl')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('errorMessage');
    }

    form.get('testControl')?.setValue(null);
    form.updateValueAndValidity();
    expect(form.get('testControl')?.errors).toBe(null);

    form.get('testControl')?.setValue(0);
    form.updateValueAndValidity();
    expect(form.get('testControl')?.value).toBe(0);
    errors = form.get('testControl')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('errorMessage');
    }
  });

  it('should check the sub-band bandwidth is not below the continuum bandwidth limit', () => {
    const bandwidthScale: ConfigurationDropDownValue = {
      type: 'frequency',
      value: 'kHz',
      operator: '*',
      multiplier: 1000
    };
    const fb: UntypedFormBuilder = new UntypedFormBuilder();
    const form: UntypedFormGroup = fb.group({
      'fieldBandwidth':  new UntypedFormControl(null),
      'fieldNoOfSubBands':  new UntypedFormControl(null)
    }, { validators: [service.subBandBandwidthValidator(bandwidthScale)]});

    form.get('fieldBandwidth')?.setValue(50);
    form.get('fieldNoOfSubBands')?.setValue(5);
    form.updateValueAndValidity();

    let errors: ValidationErrors | null | undefined = form.get('fieldNoOfSubBands')?.errors;
    if (errors) {
      const keyError = Object.keys(errors)[0];
      expect(keyError).toBe('subBandBandwidthError');
    } else {
      throw new Error('expected error in validation');
    }

    form.get('fieldBandwidth')?.setValue(500);
    form.get('fieldNoOfSubBands')?.setValue(5);
    form.updateValueAndValidity();

    errors = form.get('fieldNoOfSubBands')?.errors;
    expect(errors).toBe(null);
  });
});
