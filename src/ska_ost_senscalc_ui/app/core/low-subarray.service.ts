import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import {
  DefaultService,
  LowSubarraysResponse
} from '../generated/api/low';

@Injectable({
  providedIn: 'root'
})
export class LowSubarrayService implements OnDestroy {
  // populated with the list of subarray objects as the response is returned
  subarrays$ = new BehaviorSubject<Array<LowSubarraysResponse>>([]);

  // flag that is set if the subarray query fails
  subQueryFailed$ = new BehaviorSubject<boolean>(false);

  // list of subscriptions to be unsubscribed on service destruction
  private subscriptions: Subscription[] = [];

  constructor(private lowApi: DefaultService) {
    // custom subarray to be appended to any successful query response
    const customSubarray: LowSubarraysResponse = {
      name: 'Custom',
      label: 'Custom',
      n_stations: 512
    };

    // populate the subarrays attribute, setting the subQueryFailed flag if
    // any error occurs.
    const subarrays$ = lowApi.skaOstSenscalcLowApiSubarrays();
    this.subscriptions.push(
      subarrays$.subscribe({
        next: (value) => this.subarrays$.next(value.concat(customSubarray)),
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        error: (_) => this.subQueryFailed$.next(true)
      })
    );
  }

  private dispose() {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  ngOnDestroy() {
    this.dispose();
  }
}
