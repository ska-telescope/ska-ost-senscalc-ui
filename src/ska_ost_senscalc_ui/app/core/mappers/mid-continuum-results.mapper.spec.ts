import {ContinuumResults} from "../types";
import {
  midContinuumResponseToSensitivityResultsWithoutWeighting,
  midContinuumResponseToSensitivityFullResults,
  midContinuumResponseToSensitivityResultsNoneGaussian,
  midContinuumResponseToIntegrationTimeFullResults,
  midContinuumResponseToIntegrationTimeResultsNoneGaussian, midContinuumResponseToIntegrationTimeResultsWithoutWeighting
} from "./mid-continuum-results.mapper";
import constants from "../../shared/utils/constants";
import {
  MidContinuumSensitivityResponse,
  MidContinuumWeightingResult,
  MidWeightingSubbandResult
} from "../../generated/api/mid";

const continuumSensitivityCalculateResult: MidContinuumSensitivityResponse = {
  continuum_sensitivity: {value: 2.567e-6, unit: 'Jy'},
  spectral_sensitivity: {value: 1.234e-6, unit: 'Jy'},
  spectropolarimetry_results: {
    fwhm_of_the_rmsf: {value: 5, unit: 'rad/m^2'},
    max_faraday_depth_extent: {value: 6, unit: 'rad/m^2'},
    max_faraday_depth: {value: 7, unit: 'rad/m^2'}
  },
  continuum_subband_sensitivities: [
    {sensitivity: {value: 5.6e-6, unit: 'Jy'}},
    {sensitivity: {value: 7.8e-6, unit: 'Jy'}}
  ]
};

const continuumIntegrationTimeResult: MidContinuumSensitivityResponse = {
  continuum_integration_time: {value: 2.56, unit: 's'},
  spectral_integration_time: {value: 1.23, unit: 's'},
  spectropolarimetry_results: {
    fwhm_of_the_rmsf: {value: 5, unit: 'rad/m^2'},
    max_faraday_depth_extent: {value: 6, unit: 'rad/m^2'},
    max_faraday_depth: {value: 7, unit: 'rad/m^2'}
  },
  continuum_subband_integration_times: [
    {integration_time: {value: 5.6, unit: 's'}},
    {integration_time: {value: 7.8, unit: 's'}}
  ]
};

const singleSubbandWeighting: MidWeightingSubbandResult = {
  weighting_factor: 1.5,
  confusion_noise: {value: 1.23e-6, limit_type: 'value'}, // confusion noise is near to sensitivity so result should have sensLimitReached true
  sbs_conv_factor: 4,
  beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3}
};

const continuumWeightingResult: MidContinuumWeightingResult = {
  weighting_factor: 1.5,
  confusion_noise: {value: 1.23e-6, limit_type: 'value'}, // confusion noise is near to sensitivity so result should have sensLimitReached true
  sbs_conv_factor: 4,
  beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3},
  subbands: [singleSubbandWeighting, singleSubbandWeighting]
};

const lineWeightingResult: MidContinuumWeightingResult = {
  weighting_factor: 1.2,
  confusion_noise: {value: 0.9e-6, limit_type: 'value'}, // confusion noise is near to sensitivity so result should have sensLimitReached true
  sbs_conv_factor: 2,
  beam_size: {beam_pa: 0.5, beam_min_scaled: 1.5, beam_maj_scaled: 2.5}
};

describe('The mid continuum results mapper functions for a sensitivity calculation', () => {
  it('should map the Mid continuum responses to the results with full weighting', () => {
    const expectedContinuumResults: ContinuumResults = {
      weightedContinuumSensitivity: '3.85 uJy/beam (1.50)',
      continuumConfusionNoise: '1.23 uJy/beam',
      totalContinuumSensitivity: '4.04 uJy/beam',
      continuumSynthesizedBeamSize: '10800.000" x 7200.000"',
      continuumSurfaceBrightnessSensitivity: '16.17 uK',
      weightedSpectralSensitivity: '1.48 uJy/beam (1.20)',
      spectralConfusionNoise: '900.00 nJy/beam',
      totalSpectralSensitivity: '1.73 uJy/beam',
      spectralSynthesizedBeamSize: '9000.000" x 5400.000"',
      spectralSurfaceBrightnessSensitivity: '3.47 uK',
      weightedSensitivityPerSubBand: '8.40 uJy/beam - 11.70 uJy/beam (1.50)',
      confusionNoisePerSubBand: '1.23 uJy/beam - 1.23 uJy/beam',
      totalSensitivityPerSubBand: '8.49 uJy/beam - 11.76 uJy/beam',
      synthesizedBeamSizePerSubBand: '10800.000" x 7200.000" - 10800.000" x 7200.000"',
      surfaceBrightnessSensitivityPerSubBand: '33.96 uK - 47.06 uK',
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        integrationTimesDifferent: false,
        sensLimitReached: {reached: true}
      }
    };

    expect(
      midContinuumResponseToSensitivityFullResults(continuumSensitivityCalculateResult, continuumWeightingResult, lineWeightingResult)
    ).toStrictEqual(expectedContinuumResults);
  });

  it('should map the Mid continuum responses to the results with non Gaussian weighting', () => {
    const expectedContinuumResults: ContinuumResults = {
      weightedContinuumSensitivity: '3.85 uJy/beam (1.50)',
      continuumConfusionNoise: constants.result.nonGaussianBeam,
      totalContinuumSensitivity: constants.result.nonGaussianBeam,
      continuumSynthesizedBeamSize: constants.result.nonGaussianBeam,
      continuumSurfaceBrightnessSensitivity: constants.result.nonGaussianBeam,
      weightedSpectralSensitivity: '1.48 uJy/beam (1.20)',
      spectralConfusionNoise: constants.result.nonGaussianBeam,
      totalSpectralSensitivity: constants.result.nonGaussianBeam,
      spectralSynthesizedBeamSize: constants.result.nonGaussianBeam,
      spectralSurfaceBrightnessSensitivity: constants.result.nonGaussianBeam,
      weightedSensitivityPerSubBand: '5.60 uJy/beam - 7.80 uJy/beam (1.00)',
      confusionNoisePerSubBand: constants.result.nonGaussianBeam,
      totalSensitivityPerSubBand: constants.result.nonGaussianBeam,
      synthesizedBeamSizePerSubBand: constants.result.nonGaussianBeam,
      surfaceBrightnessSensitivityPerSubBand: constants.result.nonGaussianBeam,
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      midContinuumResponseToSensitivityResultsNoneGaussian(continuumSensitivityCalculateResult, continuumWeightingResult, lineWeightingResult)
    ).toStrictEqual(expectedContinuumResults);
  });

  it('should map the Mid continuum responses to the results when weighting is not available', () => {
  const expectedContinuumResults: ContinuumResults = {
    weightedContinuumSensitivity: '2.57 uJy/beam (1.00)',
    continuumConfusionNoise: constants.result.noBeamAvailable,
    totalContinuumSensitivity: constants.result.noBeamAvailable,
    continuumSynthesizedBeamSize: constants.result.noBeamAvailable,
    continuumSurfaceBrightnessSensitivity: constants.result.noBeamAvailable,
    weightedSpectralSensitivity: '1.23 uJy/beam (1.00)',
    spectralConfusionNoise: constants.result.noBeamAvailable,
    totalSpectralSensitivity: constants.result.noBeamAvailable,
    spectralSynthesizedBeamSize: constants.result.noBeamAvailable,
    spectralSurfaceBrightnessSensitivity: constants.result.noBeamAvailable,
    weightedSensitivityPerSubBand: '5.60 uJy/beam - 7.80 uJy/beam (1.00)',
    confusionNoisePerSubBand: constants.result.noBeamAvailable,
    totalSensitivityPerSubBand: constants.result.noBeamAvailable,
    synthesizedBeamSizePerSubBand: constants.result.noBeamAvailable,
    surfaceBrightnessSensitivityPerSubBand: constants.result.noBeamAvailable,
    spectropolarimetryResults: {
      fwhmOfTheRMSF: '5.0 rad/m^2',
      maximumFaradayDepth: '7.0 rad/m^2',
      maximumFaradayDepthExtent: '6.0 rad/m^2'
    },
    warnings: {
      integrationTimesDifferent: false,
      sensLimitReached: {reached: false}
    }
  };

  expect(
    midContinuumResponseToSensitivityResultsWithoutWeighting(continuumSensitivityCalculateResult)
  ).toStrictEqual(expectedContinuumResults);
});
});

describe('The mid continuum results mapper functions for a integration time calculation', () => {
  it('should map the Mid continuum responses to the results with full weighting', () => {
    const expectedContinuumResults: ContinuumResults = {
      continuumIntegrationTime: '2.56 s',
      spectralIntegrationTime: '1.23 s',
      integrationTimePerSubBand: '5.60 s - 7.80 s',
      continuumConfusionNoise: '1.23 uJy/beam',
      continuumSynthesizedBeamSize: '10800.000" x 7200.000"',
      spectralConfusionNoise: '900.00 nJy/beam',
      spectralSynthesizedBeamSize: '9000.000" x 5400.000"',
      confusionNoisePerSubBand: '1.23 uJy/beam - 1.23 uJy/beam',
      synthesizedBeamSizePerSubBand: '10800.000" x 7200.000" - 10800.000" x 7200.000"',
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      midContinuumResponseToIntegrationTimeFullResults(1e6, continuumIntegrationTimeResult, continuumIntegrationTimeResult, continuumWeightingResult, lineWeightingResult)
    ).toStrictEqual(expectedContinuumResults);
  });

  it('should map the Mid continuum responses to the results with non Gaussian weighting', () => {
    const expectedContinuumResults: ContinuumResults = {
      continuumIntegrationTime: '2.56 s',
      spectralIntegrationTime: '1.23 s',
      integrationTimePerSubBand: '5.60 s - 7.80 s',
      continuumConfusionNoise: constants.result.nonGaussianBeam,
      continuumSynthesizedBeamSize: constants.result.nonGaussianBeam,
      spectralConfusionNoise: constants.result.nonGaussianBeam,
      spectralSynthesizedBeamSize: constants.result.nonGaussianBeam,
      confusionNoisePerSubBand: constants.result.nonGaussianBeam,
      synthesizedBeamSizePerSubBand: constants.result.nonGaussianBeam,
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      midContinuumResponseToIntegrationTimeResultsNoneGaussian(continuumIntegrationTimeResult, continuumIntegrationTimeResult)
    ).toStrictEqual(expectedContinuumResults);
  });

  it('should map the Mid continuum responses to the results when weighting is not available', () => {
  const expectedContinuumResults: ContinuumResults = {
    continuumIntegrationTime: '2.56 s',
    spectralIntegrationTime: '1.23 s',
    integrationTimePerSubBand: '5.60 s - 7.80 s',
    continuumConfusionNoise: constants.result.noBeamAvailable,
    continuumSynthesizedBeamSize: constants.result.noBeamAvailable,
    spectralConfusionNoise: constants.result.noBeamAvailable,
    spectralSynthesizedBeamSize: constants.result.noBeamAvailable,
    confusionNoisePerSubBand: constants.result.noBeamAvailable,
    synthesizedBeamSizePerSubBand: constants.result.noBeamAvailable,
    spectropolarimetryResults: {
      fwhmOfTheRMSF: '5.0 rad/m^2',
      maximumFaradayDepth: '7.0 rad/m^2',
      maximumFaradayDepthExtent: '6.0 rad/m^2'
    },
    warnings: {
      integrationTimesDifferent: false,
      sensLimitReached: {reached: false}
    }
  };

  expect(
    midContinuumResponseToIntegrationTimeResultsWithoutWeighting(continuumIntegrationTimeResult)
  ).toStrictEqual(expectedContinuumResults);
});
});
