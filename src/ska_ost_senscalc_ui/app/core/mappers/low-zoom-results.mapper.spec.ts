import {LowSingleWeightingZoomResult, LowSingleZoomSensitivityResponse} from "../../generated/api/low";
import {ZoomResults} from "../types";
import {
  lowZoomResponseToSensitivityResultsWithoutWeighting,
  lowZoomResponseToSensitivityFullResults,
  lowZoomResponseToSensitivityResultsNoneGaussian
} from "./low-zoom-results.mapper";
import constants from "../../shared/utils/constants";

const zoomCalculateResult: LowSingleZoomSensitivityResponse[] = [{
  freq_centre: {value: 100, unit: 'GHz'},
  spectral_sensitivity: {value: 1.234, unit: 'uJy'},
  spectropolarimetry_results: {
    fwhm_of_the_rmsf: {value: 5, unit: 'rad/m^2'},
    max_faraday_depth_extent: {value: 6, unit: 'rad/m^2'},
    max_faraday_depth: {value: 7, unit: 'rad/m^2'}
  },
  warning: 'test error'
}];

const zoomWeightingResult: LowSingleWeightingZoomResult[] = [{
  freq_centre: {value: 100, unit: 'GHz'},
  weighting_factor: 1.5,
  confusion_noise: {value: 1.23e-6, limit_type: 'value'}, // confusion noise is near to sensitivity so result should have sensLimitReached true
  sbs_conv_factor: 4,
  beam_size: {beam_pa: 1, beam_min_scaled: 2, beam_maj_scaled: 3}
}];

describe('The low zoom results mapper functions', () => {
  it('should map the Low zoom responses to the results with full weighting', () => {
    const expectedZoomResults: ZoomResults = {
      weightedSpectralSensitivity: '1.85 uJy/beam (1.50)',
      spectralConfusionNoise: '1.23 uJy/beam',
      totalSpectralSensitivity: '2.22 uJy/beam',
      spectralSynthesizedBeamSize: '10800.0" x 7200.0"',
      spectralSurfaceBrightnessSensitivity: '8.89 uK',
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        errorMsg: 'Warning: test error',
        integrationTimesDifferent: false,
        sensLimitReached: {reached: true}
      }
    };

    expect(
      lowZoomResponseToSensitivityFullResults(zoomCalculateResult, zoomWeightingResult)
    ).toStrictEqual(expectedZoomResults);
  });

  it('should map the Low zoom responses to the results with non Gaussian weighting', () => {
    const expectedZoomResults: ZoomResults = {
      weightedSpectralSensitivity: '1.85 uJy/beam (1.50)',
      spectralConfusionNoise: constants.result.nonGaussianBeam,
      totalSpectralSensitivity: constants.result.nonGaussianBeam,
      spectralSynthesizedBeamSize: constants.result.nonGaussianBeam,
      spectralSurfaceBrightnessSensitivity: constants.result.nonGaussianBeam,
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        errorMsg: 'Warning: test error',
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      lowZoomResponseToSensitivityResultsNoneGaussian(zoomCalculateResult, zoomWeightingResult)
    ).toStrictEqual(expectedZoomResults);
  });

    it('should map the Low zoom responses to the results when weighting is not available', () => {
    const expectedZoomResults: ZoomResults = {
      weightedSpectralSensitivity: '1.23 uJy/beam (1.00)',
      spectralConfusionNoise: constants.result.noBeamAvailable,
      totalSpectralSensitivity: constants.result.noBeamAvailable,
      spectralSynthesizedBeamSize: constants.result.noBeamAvailable,
      spectralSurfaceBrightnessSensitivity: constants.result.noBeamAvailable,
      spectropolarimetryResults: {
        fwhmOfTheRMSF: '5.0 rad/m^2',
        maximumFaradayDepth: '7.0 rad/m^2',
        maximumFaradayDepthExtent: '6.0 rad/m^2'
      },
      warnings: {
        errorMsg: 'Warning: test error',
        integrationTimesDifferent: false,
        sensLimitReached: {reached: false}
      }
    };

    expect(
      lowZoomResponseToSensitivityResultsWithoutWeighting(zoomCalculateResult)
    ).toStrictEqual(expectedZoomResults);
  });
});
