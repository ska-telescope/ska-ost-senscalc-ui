import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MidCalculatorService } from './mid-calculator.service';
import * as config from '../../assets/configuration.json';

describe('MidCalculatorService', () => {
  let service: MidCalculatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, FormsModule, ReactiveFormsModule]
    });
    service = TestBed.inject(MidCalculatorService);
    service.setConfig(config);
  });

  it('should create advanced mode', () => {
    const observingModeValue = 'advanced';
    service.setObservingModeValue(observingModeValue);
    expect(service).toBeTruthy();
    const currentObservingMode = service.getObservingMode();
    expect(currentObservingMode).toEqual({'_value': 'advanced', 'closed': false, 'currentObservers': [], 'hasError': false, 'isStopped': false, 'observers': [], 'thrownError': null});
  });

  it('getDisplayedTaper', () => {
    expect(service.getDisplayedTaper(0)).toBe('No tapering');
    expect(service.getDisplayedTaper(100)).toBe('100.000');
    expect(service.getDisplayedTaper(100, 2.8)).toBe('50.000');
    expect(service.getDisplayedTaper(-124, 2.8)).toBe('-62.000');
    expect(service.getDisplayedTaper(280, 0)).toBe('280.000');
  });
});
