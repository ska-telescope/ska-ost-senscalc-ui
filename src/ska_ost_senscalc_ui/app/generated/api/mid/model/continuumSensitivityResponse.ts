/**
 * SKA MID Sensitivity Calculator API
 * The SKA MID Sensitivity Calculator API offers two modes: continuum and zoom.  Each of these has an API resource to calculate a sensitivity or integration time for an observation,  and another to retrieve information relating to image weighting (beam-size and relative sensitivity).  There is also a resource for obtaining subarray information. 
 *
 * The version of the OpenAPI document: 10.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { MidSpectropolarimetryResults } from './spectropolarimetryResults';
import { MidIntegrationTimeWithUnit } from './integrationTimeWithUnit';
import { MidImageSensitivity } from './imageSensitivity';
import { MidSingleSubbandIntegrationTimeResponse } from './singleSubbandIntegrationTimeResponse';
import { MidSingleSubbandSensitivityResponse } from './singleSubbandSensitivityResponse';


export interface MidContinuumSensitivityResponse { 
    continuum_sensitivity?: MidImageSensitivity;
    continuum_integration_time?: MidIntegrationTimeWithUnit;
    spectral_sensitivity?: MidImageSensitivity;
    spectral_integration_time?: MidIntegrationTimeWithUnit;
    continuum_subband_sensitivities?: Array<MidSingleSubbandSensitivityResponse>;
    continuum_subband_integration_times?: Array<MidSingleSubbandIntegrationTimeResponse>;
    spectropolarimetry_results?: MidSpectropolarimetryResults;
}

