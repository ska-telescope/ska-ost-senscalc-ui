/**
 * SKA LOW Sensitivity Calculator API
 * The SKA LOW Sensitivity Calculator API offers three modes: continuum, zoom and PSS.  Each of these has an API resource to calculate a sensitivity for an observation. Continuum and zoom have another to retrieve information relating to image weighting (beam-size and relative sensitivity).  There is also a resource for obtaining subarray information. 
 *
 * The version of the OpenAPI document: 10.0.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { LowConfusionNoise } from './confusionNoise';
import { LowBeamSize } from './beamSize';
import { LowFrequencyWithUnit } from './frequencyWithUnit';


export interface LowSingleWeightingZoomResult { 
    /**
     * Weighting correction factor
     */
    weighting_factor: number;
    /**
     * Surface-brightness-sensitivity conversion factor
     */
    sbs_conv_factor: number;
    confusion_noise: LowConfusionNoise;
    beam_size: LowBeamSize;
    freq_centre?: LowFrequencyWithUnit;
}

