export interface LowFormInput {
  common: LowCommonInput;
  continuum?: LowContinuumInput
  zoom?: LowZoomInput
  pss?: LowPssInput
}

export interface LowCommonInput {
  num_stations: number;
  subarray: 'LOW_AA05_all' | 'LOW_AA1_all' | 'LOW_AA2_all' | 'LOW_AA2_core_only' | 'LOW_AAstar_all' | 'LOW_AAstar_core_only' | 'LOW_AA4_all' | 'LOW_AA4_core_only' | 'Custom',
  ra_str: string;
  dec_str: string;
  min_elevation: number;
}

export interface LowContinuumInput {
  duration: number;
  frequency: number;
  bandwidth: number;
  averaging: number;
  imageWeighting: 'natural' | 'uniform' | 'briggs' ;
  robust?: -2 | -1 | 0 | 1 | 2;
  spectralMode: 'continuum' | 'line' ;
  nSubbands: number;
}

export interface LowZoomInput {
  duration: number;
  frequency: number;
  totalBandwidthKhz: number;
  spectralResolutionHz?: number;
  spectralAveraging?: number
  effectiveSpectralResolutionHz?: number
  imageWeighting: 'natural' | 'uniform' | 'briggs' ;
  robust?: -2 | -1 | 0 | 1 | 2;
}

export interface LowPssInput {
  pulsePeriod: number;
  pulseWidth: number;
  duration: number;
  frequency: number;
  bandwidth: number;
  chanelWidth: number;
  dispersionMeasure: number;
}
