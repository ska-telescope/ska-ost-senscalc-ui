import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LowSensitivityCalculatorRoutingModule } from './low-sensitivity-calculator-routing.module';
import { LowCalculatorComponent } from './low-calculator/low-calculator.component';
import { ContinuumPanelComponent } from './continuum-panel/continuum-panel.component';
import { ContinuumResultsComponent } from './continuum-panel/continuum-results/continuum-results.component';
import { NgMaterialModule } from '../ng-material/ng-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from "@angular/material/form-field";
import {ZoomPanelComponent} from "./zoom-panel/zoom-panel.component";
import {ZoomResultsComponent} from "./zoom-panel/zoom-results/zoom-results.component";
import {PssPanelComponent} from "./pss-panel/pss-panel.component";
import {PssResultsComponent} from "./pss-panel/pss-results/pss-results.component";

@NgModule({
  declarations: [
    LowCalculatorComponent,
    ContinuumPanelComponent,
    ContinuumResultsComponent,
    ZoomPanelComponent,
    ZoomResultsComponent,
    PssPanelComponent,
    PssResultsComponent
  ],
  imports: [
    CommonModule,
    LowSensitivityCalculatorRoutingModule,
    NgMaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    LowCalculatorComponent,
    ContinuumPanelComponent,
    ContinuumResultsComponent,
    ZoomPanelComponent,
    ZoomResultsComponent
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' }
    }
  ]
})
export class LowSensitivityCalculatorModule { }
