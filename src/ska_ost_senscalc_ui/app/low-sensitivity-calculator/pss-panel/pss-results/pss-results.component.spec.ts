import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';

import { PssResultsComponent } from './pss-results.component';
import { AppConfiguration } from '../../../app-configuration';
import config from '../../../../assets/configuration.json';

describe('PssResultsComponent', () => {
  let component: PssResultsComponent;
  let fixture: ComponentFixture<PssResultsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PssResultsComponent
      ],
      imports: [
        MatCardModule
      ],
      providers: [
        { provide: AppConfiguration, useValue: {  getConfig: () => config } }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PssResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit should have default values', () => {
    component.ngOnInit();
    expect(component.parametersChanged).toBe(false);
  });

  it('should reset the form by removing the previous sensitivities', () => {
    component.resetForm();
    expect(component.lowPssResults).toBe(undefined);
  });

  it('should display param change', () => {
    component.lowPssResults = {warnings: {}, sensitivity: '1'};
    component.parametersChanged = false;
    component.paramChange();
    expect(component.parametersChanged).toBe(true);
  });

});
