import {AfterViewInit, Component, Input, OnInit, ViewChild} from "@angular/core";
import {LowPssInput} from '../types';
import {AbstractControl, UntypedFormBuilder, UntypedFormGroup, Validators} from '@angular/forms';
import {ValidatorsService} from '../../core/validators.service';
import {PssResultsComponent} from "./pss-results/pss-results.component";
import {PssResults} from "../../core/types";

@Component({
  selector: 'app-pss-panel',
  templateUrl: './pss-panel.component.html',
  styleUrls: ['./pss-panel.component.scss']
})
export class PssPanelComponent implements OnInit, AfterViewInit {

  @Input() lowPssResults?: PssResults | null;

  @ViewChild(PssResultsComponent) output!: PssResultsComponent;

  formInitialValues: any;
  pssForm: UntypedFormGroup;

  centralFrequencyLimits = {min: 50, max: 350};

  constructor(
    private validatorService: ValidatorsService,
    private fb: UntypedFormBuilder
  ) {
    this.pssForm = this.fb.group({
      pulsePeriod: [33, [Validators.required, this.validatorService.validatorGeneral()]],
      intrinsicPulseWidth: [0.004, [Validators.required,
                                    this.validatorService.validatorGeneral(),
                                    this.validatorService.validatorPulseWidth()]],
      integrationTime: [1, [Validators.required, this.validatorService.validatorGeneral()]],
      centralFrequency: [200, [Validators.required,
                               this.validatorService.validatorNumberMinMax(this.centralFrequencyLimits.min, this.centralFrequencyLimits.max),
                               this.validatorService.lowPssBandwidthValidator()]],
      totalBandwidth: [118.52, []],
      channelWidth: [14.5, []],
      dispersionMeasure: [14, [Validators.required, this.validatorService.minimumValue(0)]]
    });
  }

  ngOnInit(): void {
    this.channelWidth?.disable();
    this.totalBandwidth?.disable();
  }

  ngAfterViewInit(): void {
    // Save the initial values for reset purposes.
    this.formInitialValues = this.pssForm.value;
  }

  get pulsePeriod() {
    return this.pssForm.get('pulsePeriod');
  }

  get intrinsicPulseWidth() {
    return this.pssForm.get('intrinsicPulseWidth');
  }

  get integrationTime() {
    return this.pssForm.get('integrationTime');
  }

  get centralFrequency() {
    return this.pssForm.get('centralFrequency');
  }

  get totalBandwidth() {
    return this.pssForm.get('totalBandwidth');
  }

  get channelWidth() {
    return this.pssForm.get('channelWidth');
  }

  get dispersionMeasure() {
    return this.pssForm.get('dispersionMeasure');
  }

  paramChange() {
    if (this.output) {
      this.output.paramChange();
    }
  }

  pulseParamChange() {
    // Validate associated pulse fields.
    this.pulsePeriod?.markAsTouched();
    this.pulsePeriod?.updateValueAndValidity();
    this.intrinsicPulseWidth?.markAsTouched();
    this.intrinsicPulseWidth?.updateValueAndValidity();
    this.paramChange();
  }

  centralFrequencyChange() {
    this.centralFrequency?.markAsTouched();
    this.centralFrequency?.updateValueAndValidity();
    this.paramChange();
  }

  getErrorMessage(control: AbstractControl | null) {
    return this.validatorService.getErrorMessage(control);
  }

  resetForm() {
    this.pssForm.reset(this.formInitialValues);
    this.pssForm.markAsUntouched();
    this.output?.resetForm();
  }

  clearResultPanel() {
    this.loadingResults(false);
  }

  loadingResults(show: boolean) {
    this.output.loadingResults(show);
  }

  getFormData(): LowPssInput {
    const data: LowPssInput = {
      pulsePeriod: this.pulsePeriod!.value,
      pulseWidth: this.intrinsicPulseWidth!.value,
      duration: this.integrationTime!.value,
      frequency: this.centralFrequency!.value,
      bandwidth: this.totalBandwidth!.value,
      chanelWidth: this.channelWidth!.value,
      dispersionMeasure: this.dispersionMeasure!.value
    };

    return data;
  }
}
