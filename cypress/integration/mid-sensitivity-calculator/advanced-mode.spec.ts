import responses from '../../fixtures/responses'
// This spec covers tests for using the advanced mode

describe('Calculate sensitivity using advanced mode', () => {
    // This is the default page of the application execute before every test
    beforeEach(() => {
        cy.visit('/');
        cy.wait(800);
        cy.getByLabel('Right Ascension').clear().type(responses.target.ra);
        cy.getByLabel('Declination').clear().type(responses.target.dec);
    })

    it('Verify sensitivity and then after enabling advanced mode', () => {
        cy.getByLabel('Subarray Configuration').click().get('mat-option').contains(/^\sAA4\s*$/).click()
        cy.openPanel('#continuumExpansionPanel')
        cy.contains("Calculate").click();
        cy.wait(300);
        // verify result output widget is rendered
        cy.get('app-continuum-results > .mat-card').should('be.visible');
        // verify title of the result output
        cy.get('.mat-card > .mat-card-title').contains('Results');
        // verify result output content
        cy.get('#weightedContSensitivity').should('be.visible');
        cy.get('#weightedContSensitivity').contains(responses.continuum.sensitivity);
        cy.get('#contSynthBeamSize').should('be.visible');
        cy.get('#contSynthBeamSize').contains(responses.continuum.beamSize);
        // enable advanced mode and check if inputs exist
        cy.get('label').contains('system').should('not.exist');
        cy.get('#mat-slide-toggle-1 > .mat-slide-toggle-label').click()
        cy.get('label').contains('system').should('be.visible')
        //change value for first advanced mode input
        cy.getByLabel('η.*system').type(responses.advanced_mode.eta_system)
        //recalculate sensitivity
        cy.contains("Calculate").click()
        cy.get('#weightedContSensitivity', { timeout: 10000 }).contains(responses.advanced_mode.sensitivity);
    })

    it('Verify advanced mode UI validation is working', () => {
        //enable advanced mode
        cy.get('#mat-slide-toggle-1 > .mat-slide-toggle-label').click()

        // There should be no errors displayed at the start
        cy.get('mat-error').should('not.exist')
        // Enable the other eta fields rather than eta system
        cy.get('#mat-slide-toggle-4 > .mat-slide-toggle-label').click()
        //change value for first advanced mode input
        cy.getByLabel('η.*pointing').type(2)
        cy.getByLabel('η.*coherence').click()
        //expect to see error
        cy.getErrorByLabel('η.*pointing').should('be.visible')
        cy.getByLabel('η.*pointing').clear()
        cy.getByLabel('η.*pointing').type('-1')
        cy.getByLabel('η.*coherence').click()
        cy.getErrorByLabel('η.*pointing').should('be.visible')
        cy.getByLabel('η.*pointing').clear()
        cy.getByLabel('η.*pointing').type('1.1')
        cy.getByLabel('η.*coherence').click()
        cy.getErrorByLabel('η.*pointing').should('be.visible')
        cy.getByLabel('η.*pointing').clear()
        cy.getByLabel('η.*pointing').type('1')
        cy.getByLabel('η.*coherence').click()
        cy.get('mat-error').should('not.exist')
        //move onto other inputs
        cy.getByLabel('η.*bandpass').type('-1')
        cy.getByLabel('η.*digitisation').click()
        cy.getErrorByLabel('η.*bandpass').should('be.visible')
        cy.getByLabel('η.*bandpass').clear()
        cy.getByLabel('η.*bandpass').type('0')
        cy.getByLabel('η.*digitisation').click()
        cy.getErrorByLabel('η.*bandpass').should('be.visible')
        cy.getByLabel('η.*bandpass').clear()
        cy.getByLabel('η.*bandpass').type('1')
        cy.getByLabel('η.*digitisation').click()
        cy.get('mat-error').should('not.exist')
    })

    it('Verify advanced mode UI toggles are working', () => {
        //enable advanced mode
        cy.get('#mat-slide-toggle-1 > .mat-slide-toggle-label').click()
        //check Tsys 15-m is enabled and Trcv SKA is disabled
        cy.getByLabel('T.*sys 15-m').should('be.enabled')
        cy.getByLabel('T.*rcv 15-m').should('be.disabled')
        //check Trcv SKA is enabled and Tsys SKA is disabled
        cy.get('#mat-slide-toggle-9 > .mat-slide-toggle-label').click()
        cy.getByLabel('T.*rcv 15-m').should('be.enabled')
        cy.getByLabel('T.*spl 15-m').should('be.enabled')
        //check Tsky toggles
        cy.get('#mat-slide-toggle-5 > .mat-slide-toggle-label').click()
        cy.getByLabel('T.*sky').should('be.enabled')
        cy.get('#mat-slide-toggle-6 > .mat-slide-toggle-label').click()
        cy.getByLabel('T.*sky').should('be.disabled')
        cy.getByLabel('T.*gal').should('be.enabled')
        cy.get('#mat-slide-toggle-7 > .mat-slide-toggle-label').click()
        cy.getByLabel('alpha').should('be.enabled')
        cy.getByLabel('T.*gal').should('be.disabled')
    })
});
