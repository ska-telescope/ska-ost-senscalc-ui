image: $SKA_K8S_TOOLS_DOCKER_BUILDER_IMAGE

default:
  tags:
    - k8srunner

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/.cache/Cypress"


stages:
  - dependencies
  - build
  - test
  - deploy # this is the name of the dev environment stage in the ST template
  - lint
  - publish
  - pages
  - scan
  - integration
  - staging
  - production

#Install basic dependencies using npm
install-dependencies:
  image: node:18.17.1-alpine
  stage: dependencies
  tags:
  - k8srunner
  script:
    - rm -rf build/*
    - rm -rf node_modules
    - mkdir -p build/reports
    - npm install
  cache:
    paths:
      - node_modules/
      - .cache/Cypress
    policy: push

#Run the static code analysis
lint:
  image: node:18.17.1-alpine
  stage: lint
  tags:
    - k8srunner
  script:
    - npm install
    - mkdir -p build/reports;
    - npm run code-analysis;
    - mv linting.xml build/reports/linting.xml;
  cache:
    paths:
      - node_modules/
    policy: pull
  artifacts:
    paths:
      - build
    expire_in: 7 days

# run the test coverage reports
unit-test:
  image: node:18.17.1-alpine
  stage: test
  tags:
    - k8srunner
  script:
    - npm install
    - mkdir -p build/reports;
    - npm run test:coverage;
    - mv coverage/cobertura-coverage.xml build/reports/code-coverage.xml;
    - mv junit.xml build/reports/unit-tests.xml;
  cache:
    paths:
      - node_modules/
    policy: pull
  artifacts:
    paths:
      - build
    reports:
      junit: build/reports/unit-tests.xml
    expire_in: 7 days


# Run the end to end cypress tests.
integration-test:
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  stage: test
  tags:
    - k8srunner
  variables:
    KUBE_NAMESPACE: 'ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA'
  before_script:
    - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
    - 'make help | grep k8s-test'
    - make k8s-install-chart
    - make k8s-wait
  script:
    - curl -fsSL https://deb.nodesource.com/setup_18.x | bash - && apt-get install -y nodejs
    - apt-get -qq -y install libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb
    - apt-get -y install jq
    - tmp=$(mktemp)
    - jq -n --arg BASE_URL $KUBE_HOST/$KUBE_NAMESPACE/senscalc '.baseUrl = $BASE_URL' cypress.json > "$tmp" && mv "$tmp" cypress.json
    - npm install cypress
    - npm run cypress:ci
  rules:
    - exists:
        - cypress/**/*
  cache:
    paths:
      - node_modules/
      - .cache/Cypress
    policy: pull
  environment:
    name: test/$CI_COMMIT_REF_SLUG
    kubernetes:
      namespace: ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA
    on_stop: stop-k8s-test
    auto_stop_in: 1 minute
  artifacts:
    when: always
    reports:
      junit: build/reports/*.xml
    paths:
      - cypress/screenshots
      - cypress/videos
      - build

stop-k8s-test:
  image: $SKA_K8S_TOOLS_BUILD_DEPLOY
  stage: test
  tags:
    - k8srunner
  when: manual
  variables:
    KUBE_NAMESPACE: 'ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA'
  script:
    - make k8s-uninstall-chart
    - kubectl -n $KUBE_NAMESPACE delete pods,svc,daemonsets,deployments,replicasets,statefulsets,cronjobs,jobs,ingresses,configmaps --all
    - make k8s-delete-namespace
  environment:
    name: test/$CI_COMMIT_REF_SLUG
    action: stop


# The info script in the templates is for a generic tango environment. We overwrite it to give more useful SC information
.info_script: &info_script
    - |-
      echo "Status of pods in $KUBE_NAMESPACE namespace:"
      kubectl get pods -n $KUBE_NAMESPACE -o jsonpath="{range .items[*]}{'Pod: '}{.metadata.name}:{'\n'}{'\t'}{'Image: '}{.spec.containers[*].image}{'\n'}{'\t'}{'Status: '}{.status.phase}{'\n'}{end}{'\n'}"
      echo "Addresses to connect to the deployment of the Sensitivity Calculator: "
      echo "  https://k8s.stfc.skao.int/$KUBE_NAMESPACE/senscalc/"
      echo "Backend API:"
      echo "  https://k8s.stfc.skao.int/$KUBE_NAMESPACE/senscalc/api/"

# Currently, the dev template uses ci-dev-$CI_PROJECT_NAME which means only one branch can be deployed at a time, so we overwrite this
.dev_env: &dev_env
  variables:
    KUBE_NAMESPACE: "dev-$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG"
  environment:
    name: "dev-$CI_PROJECT_NAME-$CI_COMMIT_REF_SLUG"

deploy-dev-environment:
  <<: *dev_env
  before_script:
    - KUBE_HOST=https://k8s.stfc.skao.int
  needs:
    - oci-image-build

test-dev-environment:
  <<: *dev_env

info-dev-environment:
  <<: *dev_env
  script:
    - *info_script

stop-dev-environment:
  <<: *dev_env

deploy-integration:
  before_script:
    - KUBE_HOST=https://k8s.stfc.skao.int

info-integration:
  script:
    - *info_script

deploy-staging:
  before_script:
    - KUBE_HOST=https://k8s.stfc.skao.int

info-staging:
  script:
    - *info_script


deploy-production:
  stage: production
  tags:
    - k8srunner
  image: $SKA_K8S_TOOLS_BUILD_DEPLOY
  variables:
    KUBE_NAMESPACE: prod-ost # This can't be changed or the deployment will fail due to missing permissions
    STORAGE: gp2 # AWS EBS Storage class, only this is supported
    API_BACKEND_URL: https://sensitivity-calculator.skao.int # AWS Deployment doesn't use namespaces for production
    SENSCALC_UI_URL: https://sensitivity-calculator.skao.int # AWS Deployment doesn't use namespaces for production
    K8S_SKIP_NAMESPACE: "true" # skip ns creation/deletion as we don't have the permissions
  environment:
    name: production-ska-ost-senscalc-ui
    on_stop: stop-production
  script:
    - KUBECONFIG=$AWS_KUBECONFIG_OST make k8s-install-chart-car
    - make k8s-wait
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: manual
      allow_failure: true # to make pipeline not blocking: https://docs.gitlab.com/ee/ci/yaml/README.html#whenmanual
    - if: $CI_COMMIT_TAG
      when: never

stop-production:
  stage: production
  tags:
    - k8srunner
  image: $SKA_K8S_TOOLS_BUILD_DEPLOY
  variables:
    KUBE_NAMESPACE: prod-ost # This can't be changed or the deployment will fail due to missing permissions
    K8S_SKIP_NAMESPACE: "true" # skip ns creation/deletion as we don't have the permissions
  script:
    - KUBECONFIG=$AWS_KUBECONFIG_OST make k8s-uninstall-chart
  rules:
    - if: "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
      when: manual
      allow_failure: true # to make pipeline not blocking: https://docs.gitlab.com/ee/ci/yaml/README.html#whenmanual
    - if: $CI_COMMIT_TAG
      when: never
  environment:
    name: production-ska-ost-senscalc-ui
    action: stop

include:
  # OCI
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/oci-image.gitlab-ci.yml"
  # Docs pages
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/docs.gitlab-ci.yml'
  # .post step finalisers eg: badges
  - project: "ska-telescope/templates-repository"
    file: "gitlab-ci/includes/finaliser.gitlab-ci.yml"
  # Helm Chart linting and Publish
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/helm-chart.gitlab-ci.yml'
  # Tag Based GitLab Release Management
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/release.gitlab-ci.yml'
  # deploy steps
  - project: 'ska-telescope/templates-repository'
    file: 'gitlab-ci/includes/deploy.gitlab-ci.yml'


