
Sensitivity Calculator UI
==========================

.. Overview
   Hidden toctree to manage the sidebar navigation.

.. toctree::
   :maxdepth: 1
   :caption: Overview
   :hidden:

This project serves as a user interface tool for calculating the sensitivity of both Mid and Low-Frequency Aperture Arrays.
It is intended for internal use by SKA astronomy scientists and engineers, as well as external use by instrument users applying
for observing time. The tool streamlines the deployment and operation of the telescope.

.. User Guide section
   Hidden toctree to manage the sidebar navigation.

.. toctree::
   :maxdepth: 1
   :caption: Releases

   CHANGELOG.rst

.. toctree::
   :maxdepth: 1
   :caption: User Guide
   :hidden:

   user_guide
   sensitivity_model

.. Developers section
   Hidden toctree to manage the sidebar navigation.

.. toctree::
   :maxdepth: 1
   :caption: For Developers
   :hidden:

   architecture
   technologies

.. toctree::
   :maxdepth: 1
   :caption: Confusion Noise Low

   confusionnoiselow


Project description
===================
The SKA Sensitivity Calculator UI project is an web application which provides a graphical user interface for calculating sensitivity and integration time for SKA Mid telescope.
The sensitivity and integration time calculation is done by ska-ost-senscalc project (see
`the backend project documentation <https://developer.skao.int/projects/ska-ost-senscalc/en/latest/>`_ for more details) which the UI connects to via a REST API.
